package com.intensy_t.pinowl.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentActivity;

import com.intensy_t.pinowl.Fragments.AccueilFragment;
import com.intensy_t.pinowl.Fragments.AgendaFragment;
import com.intensy_t.pinowl.Fragments.MessageFragment;
import com.intensy_t.pinowl.Fragments.NotifFragment;
import com.intensy_t.pinowl.Fragments.ProfilFragment;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch(position){
            case 0:
                AccueilFragment accueilFragment = new AccueilFragment();
                return accueilFragment;
            case 1:
                AgendaFragment agendaFragment = new AgendaFragment();
                Fragment af = (Fragment)agendaFragment;
                return af;
            case 2:
                ProfilFragment profilFragment = new ProfilFragment();
                return profilFragment;
            case 3:
                MessageFragment messageFragment = new MessageFragment();
                return messageFragment;
            case 4:
                NotifFragment notifFragment = new NotifFragment();
                return notifFragment;
            default :
                return null;
        }
    }

    @Override
    public int getCount() {
        return 5;
    }

}












