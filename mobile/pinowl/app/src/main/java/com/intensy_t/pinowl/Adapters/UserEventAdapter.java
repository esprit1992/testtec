package com.intensy_t.pinowl.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.intensy_t.pinowl.Entities.User;
import com.intensy_t.pinowl.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UserEventAdapter extends RecyclerView.Adapter<UserEventAdapter.MyViewHolder> {

    private List<User> UserEventList;
    private Context mcontext;


    public UserEventAdapter(List<User> userEventList, Context mcontext) {
        UserEventList = userEventList;
        this.mcontext = mcontext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_recycle_accueil, viewGroup, false);

        return new UserEventAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {


        User user = UserEventList.get(i);

        myViewHolder.poste_user.setText(user.getRole());
        myViewHolder.TV_nom.setText(user.getUsername());




         Picasso.get().load(UserEventList.get(i).getImgUser()).into(myViewHolder.img_profil);

    }

    @Override
    public int getItemCount() {
        return UserEventList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView img_profil;
        TextView TV_nom,poste_user;


        public MyViewHolder(View itemView) {
            super(itemView);

            img_profil   = itemView.findViewById(R.id.img_profil);
            poste_user   = itemView.findViewById(R.id.poste_user);
            TV_nom       = itemView.findViewById(R.id.TV_nom);

        }


    }
}
