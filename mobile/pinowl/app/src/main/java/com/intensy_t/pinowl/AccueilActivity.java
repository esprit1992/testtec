package com.intensy_t.pinowl;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Printer;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.intensy_t.pinowl.Adapters.SectionsPagerAdapter;

public class AccueilActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private TabLayout mtabLayout;

    private GoogleSignInClient mGoogleSignInClient;
    Toolbar mToolbar;
    DatabaseReference rootRef;
    private FirebaseAuth mAuth;
    private DatabaseReference UsersRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);

        mToolbar= findViewById(R.id.main_page_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Pinowl");
        //déclarations ViewPager
        viewPager = findViewById(R.id.TabPager);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mSectionsPagerAdapter);
        mtabLayout = findViewById(R.id.maintabs);
        mtabLayout.setupWithViewPager(viewPager);



        //Set icons in viewPager
        mtabLayout.getTabAt(0).setIcon(R.drawable.icon_home);
        mtabLayout.getTabAt(1).setIcon(R.drawable.icon_booking);
        mtabLayout.getTabAt(2).setIcon(R.drawable.icon_profile);
        mtabLayout.getTabAt(3).setIcon(R.drawable.icon_mail);
        mtabLayout.getTabAt(4).setIcon(R.drawable.icon_notif);




        //generate user token for puch notif
        mAuth = FirebaseAuth.getInstance();
        UsersRef = FirebaseDatabase.getInstance().getReference().child("users");
        String currentUserID = mAuth.getCurrentUser().getUid();
        final String deviceToken = FirebaseInstanceId.getInstance().getToken();

        UsersRef.child(currentUserID).child("device_token").setValue(deviceToken).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
               if (task.isSuccessful()){

               }
            }
        });

        //déclaration google+
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(String.valueOf(R.string.googleID))
                .requestEmail()
                .build();

        rootRef = FirebaseDatabase.getInstance().getReference();
        mGoogleSignInClient  = GoogleSignIn.getClient(this, gso);






        //reception des données des réseaux sociaux
        Intent intent = getIntent();
     /*   int etat = (int) getIntent().getSerializableExtra("etatconnexion");

        if (etat == 1){
            String email = intent.getStringExtra("email");

            Toast.makeText(AccueilActivity.this, "Confirmation Facebook :"+email, Toast.LENGTH_LONG).show();
        }else if (etat == 2){

            String Gmail = intent.getStringExtra("Gmail");

            Toast.makeText(AccueilActivity.this, "Confirmation Gmail :"+Gmail, Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(AccueilActivity.this, "Connexion direct:", Toast.LENGTH_LONG).show();
        }*/


        // fin reception des données des réseaux sociaux

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;

        //return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.logout_main){
            signOut();
        }
        return super.onOptionsItemSelected(item);
    }


    private void signOut() {
        // Firebase sign out
        mAuth.signOut();

        // Google sign out
        mGoogleSignInClient.signOut().addOnCompleteListener(AccueilActivity.this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        final AlertDialog ad = new AlertDialog.Builder(AccueilActivity.this)
                                .create();
                        ad.setCancelable(false);
                        ad.setTitle("Deconnect");
                        ad.setMessage("Are you sure to exit ??");
                        ad.setButton(DialogInterface.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ad.dismiss();
                            }
                        });
                        ad.setButton(AccueilActivity.this.getString(R.string.Yes_text), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(AccueilActivity.this, LoginActivity.class);
                                startActivity(intent);
                            }
                        });
                        ad.show();

                    }
                });
    }

    //button back system no action
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            return false;
        }
        return super.onKeyDown(keyCode, event);
    } //fin button back system no action
}
