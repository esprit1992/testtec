package com.intensy_t.pinowl;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.intensy_t.pinowl.Adapters.MessagesAdapter;
import com.intensy_t.pinowl.Entities.Messages;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatActivity extends AppCompatActivity {

    private String msgReciver_id,msgReciver_name,msgReciver_img,msgSenderID;

    private TextView userName,userLastSeen;
    private CircleImageView userImage;
    private EditText input_single_msg;
    private ImageButton send_message_btn;
    private ImageView back_tchat;
    private Toolbar tchatToolBar;


    private FirebaseAuth mAuth;
    private DatabaseReference rootRef;

    private List<Messages> messagesList = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager ;
    private MessagesAdapter messagesAdapter;


    private RecyclerView  userMsgRecycle;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mAuth =FirebaseAuth.getInstance();
        msgSenderID = mAuth.getCurrentUser().getUid();
        rootRef = FirebaseDatabase.getInstance().getReference();

        msgReciver_id = getIntent().getExtras().getString("visit_user_id").toString();
        msgReciver_name = getIntent().getExtras().getString("visit_user_name").toString();
        msgReciver_img = getIntent().getExtras().getString("visit_user_img").toString();

        InitVAR();


        userName.setText(msgReciver_name);
        Picasso.get().load(msgReciver_img).placeholder(R.drawable.avatar_pic_001).into(userImage);


        send_message_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendMessage();
            }
        });


        back_tchat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back =  new Intent(ChatActivity.this,AccueilActivity.class);
                startActivity(back);
            }
        });

    }



    private void InitVAR() {

        tchatToolBar = findViewById(R.id.single_tchat_toolbar);
        setSupportActionBar(tchatToolBar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);

        LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View actionBarView = layoutInflater.inflate(R.layout.custom_tchat_bar, null);
        actionBar.setCustomView(actionBarView);

        userName            = findViewById(R.id.custom_profil_name);
        userLastSeen        = findViewById(R.id.custom_user_lastSeen);
        userImage           = findViewById(R.id.custom_profil_image);
        input_single_msg    = findViewById(R.id.input_single_msg);
        send_message_btn    = findViewById(R.id.send_msgSingle_tchat_btn);
        userMsgRecycle      = findViewById(R.id.privte_msg_list_user);
        back_tchat          = findViewById(R.id.back_tchat);

        messagesAdapter = new MessagesAdapter(messagesList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        userMsgRecycle.setLayoutManager(linearLayoutManager);
        userMsgRecycle.setAdapter(messagesAdapter);
    }


    @Override
    protected void onStart() {
        super.onStart();

        rootRef.child("Messages").child(msgSenderID).child(msgReciver_id).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                Messages messages = dataSnapshot.getValue(Messages.class);
                messagesList.add(messages);
                messagesAdapter.notifyDataSetChanged();


                userMsgRecycle.smoothScrollToPosition(userMsgRecycle.getAdapter().getItemCount());
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }

    private void SendMessage(){

        String msgText = input_single_msg.getText().toString();

        if(TextUtils.isEmpty(msgText)){
            Toast.makeText(ChatActivity.this, "Write a message Please !", Toast.LENGTH_LONG).show();

        }else{
            String messageSenderRef = "Messages/" + msgSenderID + "/" + msgReciver_id;
            String messageReciverRef = "Messages/" + msgReciver_id + "/" + msgSenderID;

            DatabaseReference userMessageKeyRef = rootRef.child("messages").child(messageSenderRef).child(messageReciverRef).push();

            String msgPuchID = userMessageKeyRef.getKey();

            Map msgTextBodey = new HashMap();
            msgTextBodey.put("message", msgText);
            msgTextBodey.put("type", "text");
            msgTextBodey.put("from", msgSenderID);



            Map msgBodyDetails = new HashMap();
            msgBodyDetails.put(messageSenderRef + "/" + msgPuchID , msgTextBodey);
            msgBodyDetails.put(messageReciverRef + "/" + msgPuchID , msgTextBodey);

            rootRef.updateChildren(msgBodyDetails).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if(task.isSuccessful()){
                       // Toast.makeText(ChatActivity.this, "Message sent", Toast.LENGTH_LONG).show();

                    }else{
                      //  Toast.makeText(ChatActivity.this, "Message is not sent, there is an error !", Toast.LENGTH_LONG).show();
                    }
                    input_single_msg.setText("  ");

                }
            });


        }

    }
}




















