package com.intensy_t.pinowl.Fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.intensy_t.pinowl.Entities.Contact;
import com.intensy_t.pinowl.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotifFragment extends Fragment {

    private View RequestView;
    private RecyclerView myRequestList;


    private FirebaseAuth mAuth;
    private DatabaseReference TchatRequestRef, usersRef,ContactRef;
    private  String CurrentUserID;


    public NotifFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         RequestView = inflater.inflate(R.layout.fragment_notif, container, false);

        usersRef = FirebaseDatabase.getInstance().getReference().child("users");
        TchatRequestRef = FirebaseDatabase.getInstance().getReference().child("Chat Requests");
        ContactRef =  FirebaseDatabase.getInstance().getReference().child("Contacts");
        mAuth = FirebaseAuth.getInstance();
        CurrentUserID = mAuth.getCurrentUser().getUid();



         myRequestList = RequestView.findViewById(R.id.tchat_request_list_notif);
         myRequestList.setLayoutManager(new LinearLayoutManager(getContext()));

        return RequestView;
    }

 @Override
    public void onStart() {
        super.onStart();

        FirebaseRecyclerOptions options = new FirebaseRecyclerOptions.Builder <Contact>()
                .setQuery(TchatRequestRef.child(CurrentUserID), Contact.class)
                .build();

            FirebaseRecyclerAdapter<Contact,RequestViewHolder> adapter = new FirebaseRecyclerAdapter<Contact, RequestViewHolder>(options) {
                @Override
                protected void onBindViewHolder(@NonNull final RequestViewHolder holder, int position, @NonNull Contact model) {

                    holder.AccptBTN.setVisibility(View.VISIBLE);
                    holder.refuseBTN.setVisibility(View.VISIBLE);

                    final String list_user_id = getRef(position).getKey();

                    DatabaseReference getTypeRef = getRef(position).child("request_type").getRef();

                    getTypeRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            if (dataSnapshot.exists()) {
                                String type = dataSnapshot.getValue().toString();
                                if (type.equals("received")) {
                                    usersRef.child(list_user_id).addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.exists()) {
                                                if (dataSnapshot.hasChild("image")) {

                                                    final String requestUserImage = dataSnapshot.child("image").getValue().toString();
                                                    Picasso.get().load(requestUserImage).placeholder(R.drawable.avatar_pic_001).into(holder.FriendImage);
                                                }
                                                final String requestUserName = dataSnapshot.child("name").getValue().toString();

                                                final String requestUserRole = dataSnapshot.child("role").getValue().toString();

                                                holder.FriendName.setText(requestUserName);
                                                holder.FriendRole.setText("Wants to connect with you !! ");


                                                holder.itemView.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        CharSequence options[] = new CharSequence[]{
                                                                "Accpet",
                                                                "Refuse"
                                                        };

                                                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                                        builder.setTitle(requestUserName + " Tchat Request");
                                                        builder.setItems(options, new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {

                                                                if (which == 0) {
                                                                    ContactRef.child(CurrentUserID).child(list_user_id).child("Contacts").setValue("saved")
                                                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                @Override
                                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                                    if (task.isSuccessful()) {

                                                                                        ContactRef.child(list_user_id).child(CurrentUserID).child("Contacts").setValue("saved")
                                                                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                                    @Override
                                                                                                    public void onComplete(@NonNull Task<Void> task) {
                                                                                                        if (task.isSuccessful()) {
                                                                                                            TchatRequestRef.child(CurrentUserID).child(list_user_id).removeValue()
                                                                                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                                                        @Override
                                                                                                                        public void onComplete(@NonNull Task<Void> task) {
                                                                                                                            if (task.isSuccessful()) {

                                                                                                                                TchatRequestRef.child(list_user_id).child(CurrentUserID).removeValue()
                                                                                                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                                                                            @Override
                                                                                                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                                                                                                if (task.isSuccessful()) {
                                                                                                                                                    Toast.makeText(getActivity(), "New Contact added ", Toast.LENGTH_LONG).show();
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        });
                                                                                                                            }
                                                                                                                        }
                                                                                                                    });
                                                                                                        }
                                                                                                    }
                                                                                                });
                                                                                    }
                                                                                }
                                                                            });
                                                                }
                                                                if (which == 1) {
                                                                    ContactRef.child(list_user_id).child(CurrentUserID).child("Contacts").setValue("saved")
                                                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                @Override
                                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                                    if (task.isSuccessful()) {
                                                                                        TchatRequestRef.child(CurrentUserID).child(list_user_id).removeValue()
                                                                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                                    @Override
                                                                                                    public void onComplete(@NonNull Task<Void> task) {
                                                                                                        if (task.isSuccessful()) {

                                                                                                            TchatRequestRef.child(list_user_id).child(CurrentUserID).removeValue()
                                                                                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                                                        @Override
                                                                                                                        public void onComplete(@NonNull Task<Void> task) {
                                                                                                                            if (task.isSuccessful()) {
                                                                                                                                Toast.makeText(getActivity(), " Contact removed ! ", Toast.LENGTH_LONG).show();
                                                                                                                            }
                                                                                                                        }
                                                                                                                    });
                                                                                                        }
                                                                                                    }
                                                                                                });
                                                                                    }
                                                                                }
                                                                            });
                                                                }
                                                            }
                                                        });

                                                        builder.show();
                                                    }
                                                });
                                                //////////////////////////////

                                            }


                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                }
                            }


                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                }

                @NonNull
                @Override
                public RequestViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                    View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_find_friends, viewGroup,false);
                    RequestViewHolder viewHolder = new RequestViewHolder(view);


                    return viewHolder;
                }
            };


        myRequestList.setAdapter(adapter);
        adapter.startListening();

    }

    public static class RequestViewHolder extends RecyclerView.ViewHolder{

        TextView FriendName,FriendRole;
        CircleImageView FriendImage;
        Button AccptBTN, refuseBTN;

        public RequestViewHolder (@NonNull View itemView) {
            super(itemView);

            FriendName  = itemView.findViewById(R.id.friend_profil_name);
            FriendRole  = itemView.findViewById(R.id.friend_role);
            FriendImage = itemView.findViewById(R.id.friend_img);
            AccptBTN    = itemView.findViewById(R.id.request_accpt_btn);
            refuseBTN   = itemView.findViewById(R.id.request_cancel_btn);

        }
    }

}
