package com.intensy_t.pinowl.Adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.intensy_t.pinowl.Fragments.ContactFragment;
import com.intensy_t.pinowl.Fragments.GroupsFragment;
import com.intensy_t.pinowl.Fragments.TchatFragment;

public class TabsAccessorAdapter extends FragmentPagerAdapter {
    public TabsAccessorAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {

        switch(i){
            case 0:
                TchatFragment tchatFragment = new TchatFragment();
                return tchatFragment;
            case 1:
                GroupsFragment groupsFragment = new GroupsFragment();
                return groupsFragment;
            case 2:
                ContactFragment contactFragment = new ContactFragment();
                return contactFragment;

            default :
                return null;
        }

    }

    @Override
    public int getCount() {
        return 3;
    }


    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch(position){
            case 0:
                return "Tchat";
            case 1:
                return "Groups";
            case 2:
                return "Contacts";

            default :
                return null;
        }
    }
}
