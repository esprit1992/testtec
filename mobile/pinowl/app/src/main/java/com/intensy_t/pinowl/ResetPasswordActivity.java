package com.intensy_t.pinowl;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.materialtextfield.MaterialTextField;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ResetPasswordActivity extends AppCompatActivity {


    ImageView btn_reset;
    ImageView backReset;
    TextInputLayout ETresetMail;

    FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        btn_reset = findViewById(R.id.btn_reset);
        ETresetMail = findViewById(R.id.ETresetMail);
        backReset = findViewById(R.id.backBtnReset);

         mAuth = FirebaseAuth.getInstance();



         backReset.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Intent intent = new Intent(ResetPasswordActivity.this,LoginActivity.class);
                 startActivity(intent);
             }
         });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = ETresetMail.getEditText().getText().toString();


                if(TextUtils.isEmpty(email)){
                    Toast.makeText(getApplicationContext(), "Enter email address !", Toast.LENGTH_SHORT).show();
                }else {

                    mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            if(task.isSuccessful()){
                                Toast.makeText(getApplicationContext(), "Check you email account !", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(ResetPasswordActivity.this,LoginActivity.class);
                                startActivity(intent);
                            }else{
                                Toast.makeText(getApplicationContext(), "Error !! ", Toast.LENGTH_SHORT).show();
                                Log.d("Error", "EMAIL REST : "+task.getException().getMessage().toString());
                            }

                        }
                    });
                }

            }
        });




    }
}
