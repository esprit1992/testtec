package com.intensy_t.pinowl;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.materialtextfield.MaterialTextField;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Logger;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.Logger.Level;
import com.google.gson.JsonObject;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.intensy_t.pinowl.Interfaces.JsonPlaceHolerAPI;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SettingsActivity extends AppCompatActivity {


    CircleImageView  Settings_profil_img;
    MaterialTextField  Setting_Name_ET,Setting_mail_ET,Setting_role_ET,Setting_birth_ET;
    RadioGroup radioSex;
    TextView Setting_btn;

    RadioButton radioSexButton;
    ProgressDialog LoadingBar;


    RadioButton radioMale;
    RadioButton radioFemale;

    private String  currentUserID;
    private FirebaseAuth mAuth;
    private DatabaseReference rooRef;
    private StorageReference UserProfilImagesRef;
    public String imgUrlFB;
    public String imgUser;

    public final Calendar myCalendar = Calendar.getInstance();

    Toolbar mToolbar;


   private final static int Gallery = 1;

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        mAuth = FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();
        rooRef = FirebaseDatabase.getInstance().getReference();
        UserProfilImagesRef = FirebaseStorage.getInstance().getReference().child("Profile Images");


        mToolbar= findViewById(R.id.Settings_page_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Settings");




        Settings_profil_img = findViewById(R.id.Settings_profil_img);
        Setting_Name_ET = findViewById(R.id.Setting_Name_ET);
        Setting_mail_ET = findViewById(R.id.Setting_mail_ET);
        Setting_role_ET = findViewById(R.id.Setting_role_ET);
        Setting_birth_ET = findViewById(R.id.Setting_birth_ET);
        radioSex = findViewById(R.id.radioSex);
        Setting_btn = findViewById(R.id.Setting_btn);

        LoadingBar = new ProgressDialog(this);

        radioMale = findViewById(R.id.radioMale);
        radioFemale = findViewById(R.id.radioFemale);





        Settings_profil_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent GalleryIntent = new Intent();
                GalleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                GalleryIntent.setType("image/*");
                startActivityForResult(GalleryIntent, Gallery);
            }
        });


        //birthday
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        Setting_birth_ET.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View V) {
                new DatePickerDialog(SettingsActivity.this,date,myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });//fin birthday



        Setting_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdatSettings();
            }
        });


        RetriveUseerInfo();

    }



        public void RetriveUseerInfo(){

         rooRef.child("users").child(currentUserID).addValueEventListener(new ValueEventListener() {
             @Override
             public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                 if ((dataSnapshot.exists()) ) {



                     String retriveUsername = dataSnapshot.child("name").getValue().toString();
                     String retriveRole = dataSnapshot.child("role").getValue().toString();
                     String retriveMail = dataSnapshot.child("mail").getValue().toString();
                     String retriveBirth = dataSnapshot.child("birth").getValue().toString();


                     if (dataSnapshot.child("image").getValue() != null){
                         imgUrlFB = dataSnapshot.child("image").getValue().toString();
                         Picasso.get().load(imgUrlFB).into(Settings_profil_img);
                     }else {
                         Picasso.get().load(imgUrlFB).placeholder(R.drawable.avatar_pic_001).into(Settings_profil_img);

                     }

                     Setting_birth_ET.getEditText().setText(retriveBirth);
                     Setting_mail_ET.getEditText().setText(retriveMail);
                     Setting_role_ET.getEditText().setText(retriveRole);
                     Setting_Name_ET.getEditText().setText(retriveUsername);


                 }
             }


             @Override
             public void onCancelled(@NonNull DatabaseError databaseError) {

             }
         });

        }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == Gallery && resultCode == RESULT_OK && data != null){

            Uri imageUri = data.getData();

            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .start(this);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if(resultCode == RESULT_OK ){

                LoadingBar.setTitle("Setting Profil Image");
                LoadingBar.setMessage("Wait for the update of your profil image...");
                LoadingBar.setCanceledOnTouchOutside(false);
                LoadingBar.show();

                Uri resultUri = result.getUri();
                final StorageReference filePath = UserProfilImagesRef.child(currentUserID+".jpg");

                filePath.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if(task.isSuccessful()){
                            String dul = filePath.getDownloadUrl().toString();

                            filePath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>()
                            {
                                @Override
                                public void onSuccess(Uri downloadUrl)
                                {
                               final  String urlImage = downloadUrl.toString();

                                    rooRef.child("users").child(currentUserID).child("image")
                                            .setValue(urlImage)
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {

                                                    if (task.isSuccessful()) {
                                                        LoadingBar.dismiss();
                                                       // Toast.makeText(SettingsActivity.this, "Image saved in DataBase ", Toast.LENGTH_LONG).show();
                                                        RetriveUseerInfo();
                                                    } else {
                                                        LoadingBar.dismiss();
                                                        String errur = task.getException().toString();
                                                        Log.d("IMAGE", "IMAGE" + errur);
                                                        Toast.makeText(SettingsActivity.this, " " + errur, Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            });
                                }
                            });

                        }else{
                             LoadingBar.dismiss();
                            String errur = task.getException().toString();
                            Log.d("IMAGE", "IMAGE"+errur);
                            Toast.makeText(SettingsActivity.this, " "+errur, Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }


        }


    }

    private void UpdatSettings() {

        int selectedId = radioSex.getCheckedRadioButtonId();
        radioSexButton = (RadioButton) findViewById(selectedId);

        String username = Setting_Name_ET.getEditText().getText().toString().trim();
        String mail     = Setting_mail_ET.getEditText().getText().toString().trim();
        String role     = Setting_role_ET.getEditText().getText().toString().trim();
        String birth    = Setting_birth_ET.getEditText().getText().toString().trim();



        String gender   = radioSexButton.getText().toString().trim();


        if (TextUtils.isEmpty(username)) {
            Toast.makeText(SettingsActivity.this, "Enter your Name Please " , Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(mail)) {
            Toast.makeText(SettingsActivity.this, "Enter your Mail Please." , Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(role)) {
            Toast.makeText(SettingsActivity.this, "Enter your Role Please." , Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(birth)) {
            Toast.makeText(SettingsActivity.this, "Set your birthday Please." , Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(gender)) {
            Toast.makeText(SettingsActivity.this, "Set your gender Please." , Toast.LENGTH_SHORT).show();
            return;
        }

        HashMap<String ,Object> profilMap =  new HashMap<>();
        profilMap.put("uid", currentUserID);
        profilMap.put("name", username);
        profilMap.put("mail", mail);
        profilMap.put("role", role);
        profilMap.put("birth", birth);
        profilMap.put("gender", gender);


        rooRef.child("users").child(currentUserID).updateChildren(profilMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Toast.makeText(SettingsActivity.this, "Profil updated ", Toast.LENGTH_LONG).show();
                }else{
                    String errur = task.getException().toString();
                    Toast.makeText(SettingsActivity.this, " "+errur, Toast.LENGTH_LONG).show();
                }
            }
        });


        Intent intent = new Intent(SettingsActivity.this,AccueilActivity.class);
        startActivity(intent);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.100.7:3000/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

            JsonPlaceHolerAPI jsonPlaceHolderApi = retrofit.create(JsonPlaceHolerAPI.class);
            imgUser = imgUrlFB;
            final Call<ResponseBody> callsend = jsonPlaceHolderApi.addUser(role, username, mail, username, imgUser, gender, birth);
            final Call<ResponseBody> calledit = jsonPlaceHolderApi.editUser(role, username, mail, username, imgUser, gender, birth);

            String emailUser = mAuth.getCurrentUser().getEmail();
            Call<JsonObject> call = jsonPlaceHolderApi.getProf_User(emailUser);
            Log.d("Response Node ! ", "CALLLL : "+call.toString());
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    System.out.println(response.code());
                    if ((response.code() == 200) || (response.code() == 201)) {

                        calledit.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> calledit, Response<ResponseBody> response) {
                                try {
                                    if (response.code() == 201) {

                                        String s = response.body().string();
                                        System.out.println("Success update" + s);
                                    } else {
                                        String s = response.body().string();
                                        System.out.println("ERROR update" + s);
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                System.out.println("Failed to update" + t.getMessage());
                            }
                        });

                    } else {
                        callsend.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> callsend, Response<ResponseBody> response) {
                                try {
                                    if (response.code() == 201) {

                                        String s = response.body().string();
                                        System.out.println("Success post" + s);
                                    } else {
                                        String s = response.body().string();
                                        System.out.println("ERROR post" + s);
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                System.out.println("Failed to post" + t.getMessage());
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });
    }


    //set the date of birth
    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        Setting_birth_ET.getEditText().setText(sdf.format(myCalendar.getTime()));

    }

}
