package com.intensy_t.pinowl.Fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.intensy_t.pinowl.Entities.Contact;
import com.intensy_t.pinowl.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactFragment extends Fragment {


    private View ContactsView;
    private RecyclerView myContactsList;
    private DatabaseReference ContactRef,UserRef;
    private FirebaseAuth mAuth;


    String CurrentUserID;

    public ContactFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ContactsView = inflater.inflate(R.layout.fragment_contact, container, false);

        mAuth =FirebaseAuth.getInstance();
        CurrentUserID = mAuth.getCurrentUser().getUid();
        ContactRef = FirebaseDatabase.getInstance().getReference().child("Contacts").child(CurrentUserID);
        UserRef = FirebaseDatabase.getInstance().getReference().child("users");



        myContactsList = ContactsView.findViewById(R.id.contact_list);
        myContactsList.setLayoutManager(new LinearLayoutManager(getContext()));


        return ContactsView;
    }


    @Override
    public void onStart() {
        super.onStart();


        FirebaseRecyclerOptions options = new FirebaseRecyclerOptions.Builder <Contact>()
                .setQuery(ContactRef, Contact.class)
                .build();

        FirebaseRecyclerAdapter<Contact,ContactsViewHolder> adapter = new FirebaseRecyclerAdapter<Contact, ContactsViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final ContactsViewHolder holder, int position, @NonNull Contact model) {
                String UserIDs = getRef(position).getKey();
                UserRef.child(UserIDs).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.hasChild("image")){

                            String img_prof = dataSnapshot.child("image").getValue().toString();
                            String username = dataSnapshot.child("name").getValue().toString();
                            String role = dataSnapshot.child("role").getValue().toString();


                            holder.FriendName.setText(username);
                            holder.FriendRole.setText(role);
                            Picasso.get().load(img_prof).placeholder(R.drawable.avatar_pic_001).into(holder.FriendImage);
                        }else{
                            String username = dataSnapshot.child("name").getValue().toString();
                            String role = dataSnapshot.child("role").getValue().toString();

                            holder.FriendName.setText(username);
                            holder.FriendRole.setText(role);

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


            }

            @NonNull
            @Override
            public ContactsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_find_friends, viewGroup,false);
                ContactsViewHolder viewHolder = new ContactsViewHolder(view);


                return viewHolder;
            }
        };


        myContactsList.setAdapter(adapter);
        adapter.startListening();
    }


    public static class ContactsViewHolder extends RecyclerView.ViewHolder{

        TextView FriendName,FriendRole;
        CircleImageView FriendImage;

         public ContactsViewHolder(@NonNull View itemView) {
            super(itemView);

             FriendName  = itemView.findViewById(R.id.friend_profil_name);
             FriendRole  = itemView.findViewById(R.id.friend_role);
             FriendImage = itemView.findViewById(R.id.friend_img);
        }
    }




}
