package com.intensy_t.pinowl.Entities;

public class Contact  {

    public  String birth;
    public  String gender;
    public  String image;
    public  String mail;
    public  String name;
    public  String role;


    public Contact() {

    }

    public Contact(String birth, String gender, String image, String mail, String name, String role) {
        this.birth = birth;
        this.gender = gender;
        this.image = image;
        this.mail = mail;
        this.name = name;
        this.role = role;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "birth='" + birth + '\'' +
                ", gender='" + gender + '\'' +
                ", image='" + image + '\'' +
                ", mail='" + mail + '\'' +
                ", name='" + name + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
