package com.intensy_t.pinowl.Fragments;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.intensy_t.pinowl.AccueilActivity;
import com.intensy_t.pinowl.Adapters.TabsAccessorAdapter;
import com.intensy_t.pinowl.FindFriendsActivity;
import com.intensy_t.pinowl.LoginActivity;
import com.intensy_t.pinowl.R;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessageFragment extends Fragment {

    Toolbar mToolbar;
    ViewPager myViewPager;
    TabLayout myTabLayout;
    TabsAccessorAdapter myTabsAccessorAdapter;
    private GoogleSignInClient mGoogleSignInClient;
    DatabaseReference rootRef;

    private FirebaseAuth mAuth;


    public MessageFragment() {
        // Required empty public constructor
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_message, container, false);

        setHasOptionsMenu(true);


        myTabLayout  =  rootView.findViewById(R.id.main_tabs);
        myViewPager  =  rootView.findViewById(R.id.main_tabs_pager);
       // mToolbar     = rootView.findViewById(R.id.main_page_toolbar);
        myTabsAccessorAdapter = new TabsAccessorAdapter(  ((AppCompatActivity)getActivity()).getSupportFragmentManager());
        myViewPager.setAdapter(myTabsAccessorAdapter);
        myTabLayout.setupWithViewPager(myViewPager);
        myTabLayout.setTabTextColors(getResources().getColor(R.color.blanc),getResources().getColor(R.color.blanc ));


//        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
//        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Messaging");



        //déclaration google+
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(String.valueOf(R.string.googleID))
                .requestEmail()
                .build();

        rootRef = FirebaseDatabase.getInstance().getReference();
        mGoogleSignInClient  = GoogleSignIn.getClient(getActivity(), gso);




        return  rootView;
    }





    @Override
    public void onStart() {
        super.onStart();

        mAuth = FirebaseAuth.getInstance();
        mAuth.getCurrentUser();

        //test currentUser
        if(  mAuth.getCurrentUser() == null){

            Toast.makeText(getActivity(), "You have To Connect First", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
        }else{

        }
    }





    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.option_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);


    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == R.id.groupe_menu) {
            ResquestNewGroupe();
        }
        if (item.getItemId() == R.id.find_friends_menu) {
           Navigation();
        }
        return super.onOptionsItemSelected(item);
    }




    private void Navigation() {
        Intent intent = new Intent(getActivity(), FindFriendsActivity.class);
        startActivity(intent);
    }


    private void ResquestNewGroupe() {

        final AlertDialog.Builder builder =  new AlertDialog.Builder(getContext(),R.style.AlertDialog);
        builder.setTitle("Enter your groupe name please !");
        final EditText groupNameET = new EditText(getContext());
        groupNameET.setHint("new groupe");
        builder.setView(groupNameET);
        builder.setPositiveButton("Create", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String groupeName = groupNameET.getText().toString();

              if(TextUtils.isEmpty(groupeName)) {
                  Toast.makeText(getActivity(), "Please write Group Name ! ", Toast.LENGTH_LONG).show();
              }else{
                    CreateNewGroupe(groupeName);
              }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });
           builder.show();
    }





    private void CreateNewGroupe(final String groupeName) {

        rootRef.child("Groups").child(groupeName).setValue("").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Toast.makeText(getActivity(), groupeName +", is Successfuly Created", Toast.LENGTH_LONG).show();
                }
            }
        });
    }






    private void signOut() {
        // Firebase sign out
        mAuth.signOut();

        // Google sign out
        mGoogleSignInClient.signOut().addOnCompleteListener(getActivity(),
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        final AlertDialog ad = new AlertDialog.Builder(getActivity())
                                .create();
                        ad.setCancelable(false);
                        ad.setTitle("Deconnect");
                        ad.setMessage("Are you sure to exit ??");
                        ad.setButton(DialogInterface.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ad.dismiss();
                            }
                        });
                        ad.setButton(getActivity().getString(R.string.Yes_text), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(getActivity(), LoginActivity.class);
                                startActivity(intent);
                            }
                        });
                        ad.show();

                    }
                });
    }
}
