package com.intensy_t.pinowl.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.intensy_t.pinowl.Entities.Messages;
import com.intensy_t.pinowl.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MessageViewHolder> {

    private List<Messages> UserMessaesList ;
    private FirebaseAuth mAuth;
    private DatabaseReference userRef;


    public MessagesAdapter(List<Messages> UserMessaesList ){

        this.UserMessaesList = UserMessaesList;
    }


    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_msg_layout,viewGroup,false );
        mAuth = FirebaseAuth.getInstance();

        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MessageViewHolder messageViewHolder, int i) {

        String MsgSenderID = mAuth.getCurrentUser().getUid();
        Messages messages = UserMessaesList.get(i);

        String fromUserID = messages.getFrom();
        String fromMessagesType = messages.getType();

        userRef = FirebaseDatabase.getInstance().getReference().child("users").child(fromUserID);
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("image")){

                    String reciverImage = dataSnapshot.child("image").getValue().toString();
                    Picasso.get().load(reciverImage).placeholder(R.drawable.avatar_pic_001).into(messageViewHolder.ReciverProfilImage);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        if (fromMessagesType.equals("text")){

                messageViewHolder.ReciverMsgText.setVisibility(View.INVISIBLE);
                messageViewHolder.ReciverProfilImage.setVisibility(View.INVISIBLE);
                messageViewHolder.SenderMsgText.setVisibility(View.INVISIBLE);

            if(fromUserID.equals(MsgSenderID)){

                messageViewHolder.SenderMsgText.setVisibility(View.VISIBLE);
                messageViewHolder.SenderMsgText.setBackgroundResource(R.drawable.sender_msg_layout);
                messageViewHolder.SenderMsgText.setText(messages.getMessage());

            }else{

                messageViewHolder.ReciverProfilImage.setVisibility(View.VISIBLE );
                messageViewHolder.ReciverMsgText.setVisibility(View.VISIBLE);

                messageViewHolder.ReciverMsgText.setBackgroundResource(R.drawable.reciver_msg_layout);
                messageViewHolder.ReciverMsgText.setText(messages.getMessage());
            }
        }
    }

    @Override
    public int getItemCount() {
        return UserMessaesList.size();
    }




    public class  MessageViewHolder extends RecyclerView.ViewHolder{


        public TextView SenderMsgText , ReciverMsgText;
        public CircleImageView ReciverProfilImage;

        public MessageViewHolder(@NonNull View itemView) {
            super(itemView);


            SenderMsgText = itemView.findViewById(R.id.sender_msg_text);
            ReciverMsgText = itemView.findViewById(R.id.reciver_msg_text);
            ReciverProfilImage = itemView.findViewById(R.id.msg_prof_image);
        }
    }

}
