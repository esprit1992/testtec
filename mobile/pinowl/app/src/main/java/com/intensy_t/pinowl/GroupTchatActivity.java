package com.intensy_t.pinowl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.SimpleTimeZone;

public class GroupTchatActivity extends AppCompatActivity {

    Toolbar mToolbar;
    ImageButton sendMsgBtn;
    EditText UserMSGinput;
    ScrollView mScrollView;
    TextView DisplayTxtMSG;

    private FirebaseAuth mAuth;
    private DatabaseReference UsersRef, GroupNameRef, GroupMsgKeyRef;

    private String currentGroupName, currentUserId, currentUserName;
    private String currentDate, currentTime;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_tchat);

        currentGroupName = getIntent().getExtras().get("groupName").toString();
       // Toast.makeText(this, ""+currentGroupName, Toast.LENGTH_LONG).show();

        mAuth = FirebaseAuth.getInstance();
        currentUserId = mAuth.getCurrentUser().getUid();
        UsersRef = FirebaseDatabase.getInstance().getReference().child("users");
        GroupNameRef = FirebaseDatabase.getInstance().getReference().child("Groups").child(currentGroupName);


        mScrollView   = findViewById(R.id.my_scroll_view);
        UserMSGinput  = findViewById(R.id.input_group_msg);
        sendMsgBtn    = findViewById(R.id.send_msg_btn);
        DisplayTxtMSG = findViewById(R.id.group_tchat_text_dispaly);





        mToolbar = findViewById(R.id.group_tchat_bar_layout);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(currentGroupName);
        
        getUserInfo();


        sendMsgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendMsgInfoDatabase();

                UserMSGinput.setText("");
                mScrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        GroupNameRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                if(dataSnapshot.exists()){
                    DisplayMSG(dataSnapshot);
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                if(dataSnapshot.exists()){
                    DisplayMSG(dataSnapshot);
                }
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }




    private void getUserInfo() {

        UsersRef.child(currentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    currentUserName = dataSnapshot.child("name").getValue().toString();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    private void SendMsgInfoDatabase() {

        String message = UserMSGinput.getText().toString();
        String messageKey = GroupNameRef.push().getKey();
        if(TextUtils.isEmpty(message)){
            Toast.makeText(this, "Please write a message...", Toast.LENGTH_LONG).show();
        }else{
            Calendar calendarDate = Calendar.getInstance();
            SimpleDateFormat currentDateFormat = new SimpleDateFormat("MMM dd,YYYY");
            currentDate = currentDateFormat.format(calendarDate.getTime());

            Calendar calendarTime = Calendar.getInstance();
            SimpleDateFormat currentTimeFormat = new SimpleDateFormat("hh:mm  a");
            currentTime = currentTimeFormat.format(calendarTime.getTime());


            HashMap<String, Object> groupMessageKey = new HashMap<>();

            GroupNameRef.updateChildren(groupMessageKey);
            GroupMsgKeyRef = GroupNameRef.child(messageKey);


            HashMap<String, Object> messageInfoMap = new HashMap<>();
            messageInfoMap.put("name", currentUserName);
            messageInfoMap.put("message", message);
            messageInfoMap.put("date", currentDate);
            messageInfoMap.put("time", currentTime);
            GroupMsgKeyRef.updateChildren(messageInfoMap);

        }
    }



    //retrive and display all msg
    private void DisplayMSG(DataSnapshot dataSnapshot) {

        Iterator iterator = dataSnapshot.getChildren().iterator();

        while (iterator.hasNext()){
            String tchatDate = (String) ((DataSnapshot)iterator.next()).getValue();
            String tchatMSG  = (String) ((DataSnapshot)iterator.next()).getValue();
            String tchatName = (String) ((DataSnapshot)iterator.next()).getValue();
            String tchatTime = (String) ((DataSnapshot)iterator.next()).getValue();

            DisplayTxtMSG.append(tchatName+": \n"+ tchatMSG +": \n"+ tchatDate+ "    "+tchatTime+ " \n \n \n");

            mScrollView.fullScroll(ScrollView.FOCUS_DOWN);
        }
    }
}













