package com.intensy_t.pinowl.Entities;

import com.google.gson.annotations.SerializedName;

public class User {

    String username;
    String role;
    @SerializedName("email")
    String email;
    String fullname;
    String gender;
    String age;
    String imgUser;

    public User(String username, String role, String email, String fullname, String gender, String age, String imgUser) {
        this.username = username;
        this.role = role;
        this.email = email;
        this.fullname = fullname;
        this.gender = gender;
        this.age = age;
        this.imgUser = imgUser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getImgUser() {
        return imgUser;
    }

    public void setImgUser(String imgUser) {
        this.imgUser = imgUser;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", role='" + role + '\'' +
                ", email='" + email + '\'' +
                ", fullname='" + fullname + '\'' +
                ", gender='" + gender + '\'' +
                ", age='" + age + '\'' +
                ", img_profil='" + imgUser + '\'' +
                '}';
    }
}
