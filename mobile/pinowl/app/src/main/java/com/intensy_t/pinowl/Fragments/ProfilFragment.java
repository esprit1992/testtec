package com.intensy_t.pinowl.Fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.JsonObject;
import com.intensy_t.pinowl.Adapters.Event_ProfilAdapter;
import com.intensy_t.pinowl.Entities.Events;
import com.intensy_t.pinowl.Interfaces.JsonPlaceHolerAPI;
import com.intensy_t.pinowl.LoginActivity;
import com.intensy_t.pinowl.R;
import com.intensy_t.pinowl.SettingsActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ProfilFragment extends Fragment {


    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;


    ListView listView;
    CircleImageView  img_profile;
    TextView username_profil;
    TextView role_profil;
    Button btn_msg;
    ImageView settings;


    public String imgUrlProf;

    private String  currentUserID;

    private DatabaseReference rooRef;
    private StorageReference UserProfilImagesRef;



    private List<Events> EventProfil_List = new ArrayList<>();
    private Event_ProfilAdapter EPAdapter;



    public ProfilFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       View rootView =inflater.inflate(R.layout.fragment_profil, container, false);



        mAuth = FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();
        rooRef = FirebaseDatabase.getInstance().getReference();
        UserProfilImagesRef = FirebaseStorage.getInstance().getReference().child("Profile Images");


      // Log.d("CurrentUser", "UID :"+mAuth.getCurrentUser().getUid());

        listView        = rootView.findViewById(R.id.list_prof_event);
        img_profile     = rootView.findViewById(R.id.img_prof_profil);
        username_profil = rootView.findViewById(R.id.username_profil);
        role_profil     = rootView.findViewById(R.id.role_profil);
        settings        = rootView.findViewById(R.id.settings);




        //déclaration google+
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(String.valueOf(R.string.googleID))  
                .requestEmail()
                .build();


        mAuth = FirebaseAuth.getInstance();
        mGoogleSignInClient  = GoogleSignIn.getClient(getActivity(), gso);
        //fin déclaration google+



        //appel des méthodes
        getEventsProfil();
        RetriveUseerInfo();
      //  getProfilInfo();



        //listner
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 Intent intent = new Intent(getActivity(), SettingsActivity.class);
                 startActivity(intent);
            }
        });

        return  rootView;
    }




    private void getEventsProfil() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.100.9:3000/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolerAPI jsonPlaceHolderApi = retrofit.create(JsonPlaceHolerAPI.class);

        Call<List<Events>> call = jsonPlaceHolderApi.getProf_Events();

        call.enqueue(new Callback<List<Events>>() {
            @Override
            public void onResponse(Call<List<Events>> call, Response<List<Events>> response) {
                EventProfil_List = response.body();

             //   Log.d("List Event Profil", ""+EventProfil_List.get(0));


                EPAdapter = new Event_ProfilAdapter(EventProfil_List,getContext());
                EPAdapter.notifyDataSetChanged();
                listView.setAdapter(EPAdapter);

            }

            @Override
            public void onFailure(Call<List<Events>> call, Throwable t) {

                Toast.makeText(getActivity(), " Server is not responding , check your Internet connexion ! ", Toast.LENGTH_LONG).show();
            }


        });
    }


    public void RetriveUseerInfo(){

        rooRef.child("users").child(currentUserID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if ((dataSnapshot.exists()) ) {



                    String retriveUsername = dataSnapshot.child("name").getValue().toString();
                    String retriveRole = dataSnapshot.child("role").getValue().toString();



                    if (dataSnapshot.child("image").getValue() != null){
                        imgUrlProf = dataSnapshot.child("image").getValue().toString();
                        Picasso.get().load(imgUrlProf).into(img_profile);
                    }else {
                        Picasso.get().load(imgUrlProf).placeholder(R.drawable.avatar_pic_001).into(img_profile);

                    }


                    username_profil.setText(retriveUsername);
                    role_profil.setText(retriveRole);


                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }





//    private  void  getProfilInfo(){
//
//
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("http://192.168.100.9:3000/api/users/")
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        JsonPlaceHolerAPI jsonPlaceHolderApi = retrofit.create(JsonPlaceHolerAPI.class);
//        String email = mAuth.getCurrentUser().getEmail();
//
//
//        Call<JsonObject> call = jsonPlaceHolderApi.getProf_User(email);
//     //   Log.d("Response Node ! ", "CALLLL : "+call.toString());
//
//
//
//        call.enqueue(new Callback<JsonObject >() {
//            @Override
//            public void onResponse(Call<JsonObject > call, Response<JsonObject> response) {
//
//            //    Log.d("LISTAAA", ""+response.body());
//
//
//                String name = String.valueOf(response.body().get("fullname")).replaceAll("\"","");
//                name.replaceAll("\""," ");
//                String role = String.valueOf(response.body().get("role")).replaceAll("\"","");
//
//
//                String img_profil = (String.valueOf(response.body().get("imgUser"))).replaceAll("\"","");
//             //   Log.d("IMAAAGE", ""+img_profil);
//
//
//
//                username_profil.setText(name.trim());
//                role_profil.setText(role.trim());
//                Picasso.get().load(img_profil).fit().into(img_profile);
//
//
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//            //    Log.e("Dhia", ""+t.getMessage());
//                Toast.makeText(getActivity(), " No User ", Toast.LENGTH_LONG).show();
//            }
//
//        });
//
//    }





}
