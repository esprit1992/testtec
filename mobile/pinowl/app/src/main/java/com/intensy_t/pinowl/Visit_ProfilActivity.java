package com.intensy_t.pinowl;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class Visit_ProfilActivity extends AppCompatActivity {

    private String reciverUserID,CurrentState,SenderUserID;

    CircleImageView visit_img_prof_profil;
    TextView visit_username_profil,visit_role_profil;
    Button  visit_btn_msg,accept_btn_msg,refuse_btn_msg;

    private DatabaseReference UserRef,TchatRequestRef,ContactRef,NotifRef;
    private FirebaseAuth mAuth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit__profil);


        UserRef          = FirebaseDatabase.getInstance().getReference().child("users");
        TchatRequestRef  = FirebaseDatabase.getInstance().getReference().child("Chat Requests");
        ContactRef       = FirebaseDatabase.getInstance().getReference().child("Contacts");
        NotifRef         = FirebaseDatabase.getInstance().getReference().child("Notifications");
        mAuth            = FirebaseAuth.getInstance();


        reciverUserID = getIntent().getExtras().get("visit_user_id").toString();
        SenderUserID  = mAuth.getCurrentUser().getUid();

        visit_img_prof_profil = findViewById(R.id.visit_img_prof_profil);
        visit_username_profil = findViewById(R.id.visit_username_profil);
        visit_role_profil     = findViewById(R.id.visit_role_profil);
        visit_btn_msg         = findViewById(R.id.visit_btn_msg);

        accept_btn_msg = findViewById(R.id.accept_btn_msg);
        refuse_btn_msg = findViewById(R.id.refuse_btn_msg);
        CurrentState = "new";

         RetreveUserInfo();
    }

    private void RetreveUserInfo() {

        UserRef.child(reciverUserID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() && (dataSnapshot.hasChild("image"))){

                    String userIMG = dataSnapshot.child("image").getValue().toString();
                    String userName = dataSnapshot.child("name").getValue().toString();
                    String userRole = dataSnapshot.child("role").getValue().toString();


                    Picasso.get().load(userIMG).placeholder(R.drawable.avatar_pic_001).into(visit_img_prof_profil);
                    visit_username_profil.setText(userName);
                    visit_role_profil.setText(userRole);


                    ManageTchatRequest();

                }else{
                    String userName = dataSnapshot.child("name").getValue().toString();
                    String userRole = dataSnapshot.child("role").getValue().toString();

                    visit_img_prof_profil.setImageResource(R.drawable.avatar_pic_001);
                    visit_username_profil.setText(userName);
                    visit_role_profil.setText(userRole);

                    ManageTchatRequest();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void ManageTchatRequest() {

        TchatRequestRef.child(SenderUserID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.hasChild(reciverUserID)){
                    String request_type = dataSnapshot.child(reciverUserID).child("request_type").getValue().toString();
                    if(request_type.equals("sent")){

                        CurrentState = "request_sent";
                        visit_btn_msg.setText("Cancel Tchat Request");
                    }else if(request_type.equals("received")){

                        CurrentState = "request_received";

                        visit_btn_msg.setVisibility(View.INVISIBLE);

                        accept_btn_msg.setVisibility(View.VISIBLE);
                        refuse_btn_msg.setVisibility(View.VISIBLE);

                        accept_btn_msg.setEnabled(true);
                        refuse_btn_msg.setEnabled(true);


                        //accpet action
                        accept_btn_msg.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(CurrentState.equals("request_received")){
                                    AccptTachtRequest();
                                }

                            }
                        });


                        //refuse action
                        refuse_btn_msg.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                   CancelTchatRequest();
                            }
                        });

                    }
                }else{
                    ContactRef.child(SenderUserID).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            if(dataSnapshot.hasChild(reciverUserID)){
                                CurrentState = "friends";
                                visit_btn_msg.setText("Remove this contact");

                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        if(!SenderUserID.equals(reciverUserID )){

            visit_btn_msg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    visit_btn_msg.setEnabled(false);
                    if(CurrentState.equals("new")){
                        SendTchatRequest();

                    }
                    if(CurrentState.equals("firends")){
                        removeSpecifiqueContact();
                        //check the place of appel to do the remove contact
                    }

                }
            });

        }else {
            visit_btn_msg.setVisibility(View.INVISIBLE);
        }
    }



    private void AccptTachtRequest() {

        ContactRef.child(SenderUserID).child(reciverUserID).child("Contacts")
                .setValue("Saved").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){

                    ContactRef.child(reciverUserID).child(SenderUserID).child("Contacts")
                            .setValue("Saved").addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                //remove tchat request
                                TchatRequestRef.child(SenderUserID).child(reciverUserID).removeValue()
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()) {
                                                    TchatRequestRef.child(reciverUserID).child(SenderUserID).removeValue()
                                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {

                                                                    visit_btn_msg.setEnabled(true);
                                                                    CurrentState="friends";
                                                                    visit_btn_msg.setText("Remove this contact");
                                                                    refuse_btn_msg.setVisibility(View.INVISIBLE);
                                                                    accept_btn_msg.setVisibility(View.INVISIBLE);
                                                                    refuse_btn_msg.setEnabled(true);
                                                                    accept_btn_msg.setEnabled(true);
                                                                }
                                                            });
                                                }
                                            }
                                        });
                            }
                        }
                    });
                }
            }
        });
    }


    private void SendTchatRequest() {

        TchatRequestRef.child(SenderUserID).child(reciverUserID).child("request_type")
                .setValue("sent").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){

                    TchatRequestRef.child(reciverUserID).child(SenderUserID).child("request_type")
                            .setValue("received").addOnCompleteListener(new OnCompleteListener<Void>() {

                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){

                                HashMap<String,String> tchatnotificationMap =  new HashMap<>();
                                tchatnotificationMap.put("from", SenderUserID);
                                tchatnotificationMap.put("type", "request");
                                NotifRef.child(reciverUserID).push().setValue(tchatnotificationMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){

                                            visit_btn_msg.setEnabled(true);
                                            CurrentState = "request_sent";
                                            visit_btn_msg.setText("Cancel Tchat Request");
                                        }

                                    }
                                });

                            }

                        }
                    });
                }
            }
        });

    }



    private void CancelTchatRequest() {

        TchatRequestRef.child(SenderUserID).child(reciverUserID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){

                    TchatRequestRef.child(reciverUserID).child(SenderUserID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                visit_btn_msg.setEnabled(true);
                                CurrentState="new";
                                visit_btn_msg.setText("Send Message");

                            }
                        }
                    });
                }
            }
        });
    }


    private void removeSpecifiqueContact() {

        ContactRef.child(SenderUserID).child(reciverUserID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){

                    ContactRef.child(reciverUserID).child(SenderUserID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                visit_btn_msg.setEnabled(true);
                                CurrentState="new";
                                visit_btn_msg.setText("Send Message");

                            }
                        }
                    });
                }
            }
        });

    }
}
