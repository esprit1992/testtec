package com.intensy_t.pinowl;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.materialtextfield.MaterialTextField;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class SignUpActivity extends Activity {

    public String img_path;

    ImageView photo;
    EditText ETname;
    EditText ETmail;
    EditText ETpswd;
    EditText ETrole;
    EditText ETbirth;
    ImageView BTNSignUp;
    TextView backBtnSignUP;

    private final static int Gallery = 1;
    ProgressDialog LoadingBar;
    private StorageReference UserProfilImagesRef;
    private String currentUserID;


    private FirebaseAuth mAuth;
    public final Calendar myCalendar = Calendar.getInstance();

    DatabaseReference rootRef;

    String nom, mail, password, birth;

    int connxionEtat = 0;

    int REQUEST_CAMERA = 1, SELECT_FILE = 0, NO_FILE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mAuth = FirebaseAuth.getInstance();
        UserProfilImagesRef = FirebaseStorage.getInstance().getReference().child("Profile Images");
       // currentUserID = mAuth.getCurrentUser().getUid();


        photo = findViewById(R.id.photo);
        ETname = findViewById(R.id.ETname);
        ETmail = findViewById(R.id.ETmail);
        ETpswd = findViewById(R.id.ETpswd);
        ETrole = findViewById(R.id.ETrole);
        ETbirth = findViewById(R.id.ETbirth);
        BTNSignUp = findViewById(R.id.BTNSignUp);
        backBtnSignUP = findViewById(R.id.backBtnSignUP);


        rootRef = FirebaseDatabase.getInstance().getReference();


        //request de l'ajout d'une image
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            photo.setEnabled(false);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }


        //birthday
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        ETbirth.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View V) {
                new DatePickerDialog(SignUpActivity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });//fin birthday


        backBtnSignUP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });


        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectImage();

            }
        });


        BTNSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nom = ETname.getText().toString().trim();
                mail = ETmail.getText().toString().trim();
                password = ETpswd.getText().toString().trim();
                birth = ETbirth.getText().toString().trim();


                if (TextUtils.isEmpty(nom)) {
                    Toast.makeText(SignUpActivity.this, "Enter your Name Please ", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(mail)) {
                    Toast.makeText(SignUpActivity.this, "Enter your Mail Please.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(SignUpActivity.this, "Enter your Password Please.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(birth)) {
                    Toast.makeText(SignUpActivity.this, "Set your birthday Please.", Toast.LENGTH_SHORT).show();
                    return;
                }


                //check internet connexion
                boolean connected = false;
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                        connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
                    //we are connected to a network
                    connected = true;


                    mAuth.createUserWithEmailAndPassword(mail, password)
                            .addOnCompleteListener(SignUpActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {

                                    if (!task.isSuccessful()) {

                                        String deviceToken = FirebaseInstanceId.getInstance().getToken();
                                        String currentUserID = mAuth.getCurrentUser().getUid();
                                        rootRef.child("users").child(currentUserID).setValue("");

                                        rootRef.child("users").child(currentUserID).child("device_token").setValue(deviceToken);


                                        ////////////////// end device token for puch notif

                                        AlertDialog alertDialog = new AlertDialog.Builder(SignUpActivity.this).create();
                                        alertDialog.setTitle("Authentication failed !!");
                                        alertDialog.setMessage("A problem is a problem has happened !");
                                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                        alertDialog.show();

                                        Log.e("Firas :", "Erreurr : " + task.getException());

                                    } else {
                                       // Toast.makeText(SignUpActivity.this, "Authentication success.", Toast.LENGTH_SHORT).show();


                                        String currentIDUser = mAuth.getCurrentUser().getUid();
                                        String username = ETname.getText().toString().trim();
                                        String email = ETmail.getText().toString().trim();
                                        String role = ETrole.getText().toString().trim();
                                        String birth = ETbirth.getText().toString().trim();


                                        HashMap<String, String> lMap = new HashMap<>();
                                        lMap.put("uid", currentIDUser);
                                        lMap.put("name", username);
                                        lMap.put("role", role);
                                        lMap.put("birth", birth);
                                        lMap.put("mail", email);
                                        lMap.put("image", img_path);


                                        rootRef.child("users").child(currentIDUser).setValue(lMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                   // Toast.makeText(SignUpActivity.this, "Profil Created ", Toast.LENGTH_LONG).show();
                                                } else {
                                                    String errur = task.getException().toString();
                                                    Toast.makeText(SignUpActivity.this, " " + errur, Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        });

/////////////////

                                        FirebaseAuth.getInstance().getCurrentUser().sendEmailVerification();

                                        AlertDialog alertDialog = new AlertDialog.Builder(SignUpActivity.this).create();
                                        alertDialog.setTitle("Authentication success!");
                                        alertDialog.setMessage("Welcome to Pinowl");
                                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        Intent intentGoogle = new Intent(SignUpActivity.this, AccueilActivity.class);
                                                        intentGoogle.putExtra("etatconnexion", connxionEtat);
                                                        startActivity(intentGoogle);
                                                        finish();


                                                    }
                                                });
                                        alertDialog.show();


                                    }
                                }
                            });

                } else {
                    connected = false;

                    AlertDialog alertDialog = new AlertDialog.Builder(SignUpActivity.this).create();
                    alertDialog.setTitle("Connexion failed !!");
                    alertDialog.setMessage("Check your connexion Please !");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }


            }
        });


    }

    //set the date of birth
    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        ETbirth.setText(sdf.format(myCalendar.getTime()));
    }


    //ajout image profile
    private void SelectImage() {

        final CharSequence[] items = {"Camera", "Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
        builder.setTitle("Add Image");

        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (items[i].equals("Camera")) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                    NO_FILE = 1;

                } else if (items[i].equals("Gallery")) {

                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent, SELECT_FILE);
                    NO_FILE = 1;

                } else if (items[i].equals("Cancel")) {
                    dialogInterface.dismiss();
                }
            }
        });
        builder.show();

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == REQUEST_CAMERA) {


                Bundle bundle = data.getExtras();
                final Bitmap bmp = (Bitmap) bundle.get("data");
                photo.setImageBitmap(bmp);


            } else if (requestCode == SELECT_FILE) {

                Uri selectedImageUri = data.getData();
                photo.setImageURI(selectedImageUri);
                img_path = String.valueOf(selectedImageUri);


            }


        }

    }//  fin des méthodes de la recupération de l'image





}
