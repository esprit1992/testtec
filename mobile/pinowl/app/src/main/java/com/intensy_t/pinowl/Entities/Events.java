package com.intensy_t.pinowl.Entities;

import java.util.List;

public class Events {

    String name;
    String imgEvent;
    List<User> creator;
    String description;
    String venu;
    String date;


    public Events(String name, String imgEvent, List<User> creator, String description, String venu, String date) {
        this.name = name;
        this.imgEvent = imgEvent;
        this.creator = creator;
        this.description = description;
        this.venu = venu;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgEvent() {
        return imgEvent;
    }

    public void setImgEvent(String imgEvent) {
        this.imgEvent = imgEvent;
    }

    public List<User> getCreator() {
        return creator;
    }

    public void setCreator(List<User> creator) {
        this.creator = creator;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVenu() {
        return venu;
    }

    public void setVenu(String venu) {
        this.venu = venu;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Events{" +
                "name='" + name + '\'' +
                ", imgEvent='" + imgEvent + '\'' +
                ", creator='" + creator + '\'' +
                ", description='" + description + '\'' +
                ", venu='" + venu + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
