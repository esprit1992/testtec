package com.intensy_t.pinowl.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.intensy_t.pinowl.ChatActivity;
import com.intensy_t.pinowl.Entities.Contact;
import com.intensy_t.pinowl.R;
import com.squareup.picasso.Picasso;

import java.sql.SQLOutput;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class TchatFragment extends Fragment {

    private View rootView;
    private RecyclerView chatList ;
    private DatabaseReference refData,usersRef;
    private FirebaseAuth mAuth;
    private String uid;





    public TchatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_tchat, container, false);


        chatList= (RecyclerView)rootView.findViewById(R.id.chats_list);
        chatList.setLayoutManager(new LinearLayoutManager(getContext()));

        mAuth = FirebaseAuth.getInstance();
        String uid = mAuth.getCurrentUser().getUid();
        refData = FirebaseDatabase.getInstance().getReference().child("Contacts").child(uid);
        usersRef = FirebaseDatabase.getInstance().getReference().child("users");
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseRecyclerOptions<Contact> options = new FirebaseRecyclerOptions.Builder<Contact>()
                .setQuery(refData,Contact.class)
                .build();
        FirebaseRecyclerAdapter<Contact,ChatViewHolder> adapter = new FirebaseRecyclerAdapter<Contact, ChatViewHolder>(options) {

            @Override
            protected void onBindViewHolder(@NonNull final ChatViewHolder holder, int position, @NonNull Contact model) {

            final String usersIDs = getRef(position).getKey();
                final String[] retImage = {"default_IMG"};
            usersRef.child(usersIDs).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        if (dataSnapshot.hasChild("image")){

                             retImage[0] = dataSnapshot.child("image").getValue().toString();
                            Picasso.get().load(retImage[0]).placeholder(R.drawable.avatar_pic_001).into(holder.profileImage);
                        }

                        final String retName = dataSnapshot.child("name").getValue().toString();
                        final String retRole = dataSnapshot.child("role").getValue().toString();
                        holder.userName.setText(retName);
                        holder.userStatus.setText("Last Seen : "+ "\n"+"Date :"+ "Time :");


                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Intent tchatIntent = new Intent(getActivity(), ChatActivity.class);
                                tchatIntent.putExtra("visit_user_id", usersIDs);
                                tchatIntent.putExtra("visit_user_name", retName);
                                tchatIntent.putExtra("visit_user_img", retImage[0]);
                                startActivity(tchatIntent);
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    System.out.println("error");
                }
            });

            }

            @NonNull
            @Override
            public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_find_friends,viewGroup,false );
                return new ChatViewHolder(view);
            }
        };
        chatList.setAdapter(adapter);
        adapter.startListening();
    }
    public static class ChatViewHolder extends RecyclerView.ViewHolder{

        CircleImageView profileImage;
        TextView userName,userStatus;
        ImageView connected;
        public ChatViewHolder(@NonNull View itemView) {

            super(itemView);
            profileImage = itemView.findViewById(R.id.friend_img);
            connected = itemView.findViewById(R.id.friend_online_statut);
            userName = itemView.findViewById(R.id.friend_profil_name);
            userStatus = itemView.findViewById(R.id.friend_role);
        }
    }
}
