package com.intensy_t.pinowl.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.intensy_t.pinowl.Entities.Events;
import com.intensy_t.pinowl.R;

import java.util.List;

public class Event_ProfilAdapter extends BaseAdapter {


    private List<Events> Event_profilList;
    private Context context;

    public Event_ProfilAdapter(List<Events> event_profilList, Context context) {
        Event_profilList = event_profilList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return Event_profilList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.cell_list_profil, null, true);

            holder.dateEvent_Prof    = convertView.findViewById(R.id.dateEvent_Prof);
            holder.NomEvent_Prof     = convertView.findViewById(R.id.NomEvent_Prof);
            holder.CreatorEvent_Prof = convertView.findViewById(R.id.CreatorEvent_Prof);
            convertView.setTag(holder);
        }else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (ViewHolder)convertView.getTag();
        }


        holder.dateEvent_Prof.setText(Event_profilList.get(position).getDate());
        holder.NomEvent_Prof.setText(Event_profilList.get(position).getName());
        holder.CreatorEvent_Prof.setText(Event_profilList.get(position).getCreator().get(0).getUsername());

        return convertView;
    }

    private class ViewHolder {

        protected TextView dateEvent_Prof;
        protected TextView NomEvent_Prof;
        protected TextView CreatorEvent_Prof;

    }
}
