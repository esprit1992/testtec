package com.intensy_t.pinowl.Fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.intensy_t.pinowl.Adapters.EventsAdapter;
import com.intensy_t.pinowl.Adapters.UserEventAdapter;
import com.intensy_t.pinowl.Entities.Events;
import com.intensy_t.pinowl.Entities.User;
import com.intensy_t.pinowl.Interfaces.JsonPlaceHolerAPI;
import com.intensy_t.pinowl.LoginActivity;
import com.intensy_t.pinowl.QrReaderActivity;
import com.intensy_t.pinowl.R;
import com.intensy_t.pinowl.Utils.MovableFloatingActionButton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class AccueilFragment extends Fragment {

    private List<Events> EventList = new ArrayList<>();
    private List<User> UserEventList = new ArrayList<>();
    private RecyclerView recyclerView;
    private EventsAdapter mAdapter;
    private UserEventAdapter uAdapter;
    private ProgressBar progressBar;
    private GoogleSignInClient mGoogleSignInClient;
    Toolbar mToolbar;
    private FirebaseAuth mAuth;
    View rootView;

    public AccueilFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_accueil, container, false);
        setHasOptionsMenu(true);
        //mToolbar     = rootView.findViewById(R.id.app_bar_layout);
        MovableFloatingActionButton fab =rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent qrReader = new Intent(getContext(), QrReaderActivity.class);
                startActivity(qrReader);
            }
        });
//
//        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
//        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Pinowl");


        progressBar = (ProgressBar)rootView.findViewById(R.id.progressBar1);
       //declaration
        recyclerView = rootView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new EventsAdapter(EventList,getContext());

        uAdapter =  new UserEventAdapter(UserEventList, getContext());



        mAuth = FirebaseAuth.getInstance();


       //appel des méthodes
        progressBar.setVisibility(rootView.VISIBLE);
        progressBar.setVisibility(rootView.GONE);


        return rootView;
    }


    @Override
    public void onStart() {
        super.onStart();
        getDataEvents();
    }

    private void getDataEvents() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.100.9:3000/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolerAPI jsonPlaceHolderApi = retrofit.create(JsonPlaceHolerAPI.class);

        Call<List<Events>> call = jsonPlaceHolderApi.getEvents();

        call.enqueue(new Callback<List<Events>>() {
            @Override
            public void onResponse(Call<List<Events>> call, Response<List<Events>> response) {
                EventList = response.body();

                rootView.invalidate();

                for (int i =0; i < EventList.size(); i++) {
                    String name = EventList.get(i).getName();
                    List<User> creatorList = EventList.get(i).getCreator();
                    String creator = creatorList.get(0).getUsername();
                    System.out.println("creator"+creator);
                    String role = creatorList.get(0).getRole();
                    String img_profil = creatorList.get(0).getImgUser();
                    String description = EventList.get(i).getDescription();
                    String venuId = EventList.get(i).getVenu();
                    String date = EventList.get(i).getDate().toString().substring(0,3);
                    System.out.println(date);
                    String imgEvent = EventList.get(i).getImgEvent();
                    break;

                  
                }
                mAdapter = new EventsAdapter(EventList,getContext());
                mAdapter.notifyDataSetChanged();
                recyclerView.setAdapter(mAdapter);


            }

            @Override
            public void onFailure(Call<List<Events>> call, Throwable t) {
                Log.e("LOG_FIRAS ", ""+t.getMessage());
                Toast.makeText(getActivity(), "Check you Internet Connexion please !!", Toast.LENGTH_LONG).show();
            }


        });
    }

//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//
//        inflater.inflate(R.menu.option_menu, menu);
//        super.onCreateOptionsMenu(menu, inflater);
//
//
//    }


//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//
//        if (item.getItemId() == R.id.logout_menu){
//            signOut();
//        }
//        return super.onOptionsItemSelected(item);
//    }

//    private void signOut() {
//        // Firebase sign out
//        mAuth.signOut();
//
//        // Google sign out
//        mGoogleSignInClient.signOut().addOnCompleteListener(getActivity(),
//                new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        final AlertDialog ad = new AlertDialog.Builder(getActivity())
//                                .create();
//                        ad.setCancelable(false);
//                        ad.setTitle("Deconnect");
//                        ad.setMessage("Are you sure to exit ??");
//                        ad.setButton(DialogInterface.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                ad.dismiss();
//                            }
//                        });
//                        ad.setButton(getActivity().getString(R.string.Yes_text), new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                                Intent intent = new Intent(getActivity(), LoginActivity.class);
//                                startActivity(intent);
//                            }
//                        });
//                        ad.show();
//
//                    }
//                });
//    }


//    private void getUserDataEvents() {
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("http://192.168.100.9:3000/api/")
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        JsonPlaceHolerAPI jsonPlaceHolderApi = retrofit.create(JsonPlaceHolerAPI.class);
//
//        Call<List<User>> call = jsonPlaceHolderApi.getUsers();
//
//        call.enqueue(new Callback<List<User>>() {
//
//
//            @Override
//            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
//
//
//                UserEventList = response.body();
//
//
//                Toast.makeText(getActivity(), "Retrofit  User ", Toast.LENGTH_LONG).show();
//
//                for (int i =0; i < UserEventList.size(); i++) {
//                    String name = UserEventList.get(i).getUsername();
//                    String role = UserEventList.get(i).getRole();
//                    String img_profil = UserEventList.get(i).getImgUser();
//
//                    break;
//
//
//                }
//                uAdapter.notifyDataSetChanged();
//                recyclerView.setAdapter(uAdapter);
//
//
//
//
//            }
//
//            @Override
//            public void onFailure(Call<List<User>> call, Throwable t) {
//                Log.e("LOG_FIRAS ", ""+t.getMessage());
//                Toast.makeText(getActivity(), "server is not responding ", Toast.LENGTH_LONG).show();
//            }
//        });
//    }




}
