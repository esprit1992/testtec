package com.intensy_t.pinowl.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.intensy_t.pinowl.Entities.Events;
import com.intensy_t.pinowl.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.MyViewHolder> {


    private List<Events> EventList;
    private Context mcontext;


    public EventsAdapter(List<Events> eventList, Context mcontext) {
        EventList = eventList;
        this.mcontext = mcontext;
    }




    @NonNull
    @Override
    public EventsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_recycle_accueil, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EventsAdapter.MyViewHolder myViewHolder, int i) {

        Events event = EventList.get(i);

        myViewHolder.Even_Creator.setText(event.getName());
        if (event.getCreator()!= null) {
            myViewHolder.TV_nom.setText(event.getCreator().get(0).getUsername());
            myViewHolder.poste_user.setText(event.getCreator().get(0).getRole());
        }
        myViewHolder.Event_Des.setText(event.getDescription());
        myViewHolder.Event_Venu.setText(event.getVenu());
        myViewHolder.Event_Date.setText(event.getDate());




        Picasso.get().load(EventList.get(i).getImgEvent()).into(myViewHolder.img_Event);
        Picasso.get().load(event.getCreator().get(0).getImgUser()).into(myViewHolder.imgUser);

        System.out.println("role : "+ event.getCreator().get(0).getImgUser());




    }

    @Override
    public int getItemCount() {
        return EventList.size();
    }



    class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView imgUser;
        ImageView img_Event;
        TextView TV_nom,Even_Creator,Event_Venu,Event_Des,poste_user,Event_Date;


        public MyViewHolder(View itemView) {
            super(itemView);

            imgUser   = itemView.findViewById(R.id.img_profil);
            img_Event    = itemView.findViewById(R.id.img_Event);
            TV_nom       = itemView.findViewById(R.id.TV_nom);
            Even_Creator = itemView.findViewById(R.id.Even_Titre);
            Event_Venu  = itemView.findViewById(R.id.Event_Owner);
            Event_Des   = itemView.findViewById(R.id.Event_Des);
            poste_user   = itemView.findViewById(R.id.poste_user);
            Event_Date   = itemView.findViewById(R.id.Event_Date);


        }


    }


}
