package com.intensy_t.pinowl;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.github.florent37.materialtextfield.MaterialTextField;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.Credentials;
import com.google.android.gms.auth.api.credentials.CredentialsClient;
import com.google.android.gms.auth.api.credentials.IdentityProviders;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Clock;
import java.util.Arrays;

import static android.content.ContentValues.TAG;

public class LoginActivity extends Activity {


    private FirebaseAuth mAuth;
    CallbackManager callbackManager;


    DatabaseReference rootRef;


    private URL profilePictureURL;
    private String userId;


    TextInputLayout Username;
    TextInputLayout Password;
    ImageView Login;
    TextView SignUP;
    TextView forgot_password;
    ImageView FBLogo;
    ImageView LinkedLogo;



    CredentialsClient mCredentialsClient;
    private static final int RC_SAVE = 1;
    private static final String PREFS_NAME = "preferences";
    private static final String PREF_UNAME = "Username";
    private static final String PREF_PASSWORD = "Password";

    private final String DefaultUnameValue = "";
    private String UnameValue;

    private final String DefaultPasswordValue = "";
    private String PasswordValue;


    int connxionEtat = 0;



    LoginButton loginButton;
    com.facebook.login.LoginManager fbLoginManager;

    String utilisateur,mot_passe;

    @Override
    protected void onStart() {
        super.onStart();




        FirebaseUser firebaseUser = mAuth.getCurrentUser();
        rootRef = FirebaseDatabase.getInstance().getReference();


        if(firebaseUser == null){


        }else {

            Intent mainIntent = new Intent(LoginActivity.this,AccueilActivity.class);
            mainIntent.putExtra("etatconnexion", 0);
            startActivity(mainIntent);
            this.finish();
        }







    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();
        rootRef = FirebaseDatabase.getInstance().getReference();


        Username = findViewById(R.id.ETUsername);
        Password = findViewById(R.id.ETpassword);
        Login = findViewById(R.id.BTNLogin);
        SignUP = findViewById(R.id.txt);
        forgot_password = findViewById(R.id.forgot_password);

        loginButton = findViewById(R.id.login_button);


        FBLogo = findViewById(R.id.FBlogo);
        LinkedLogo = findViewById(R.id.Llogo);


        //appel méthodes
        LoginFB();
        LoginGoogle();
        initializeControls();






        //listenrs on buttons
         SignUP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(i);
            }
        });



        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent forgetPassword = new Intent(LoginActivity.this,ResetPasswordActivity.class);
                startActivity(forgetPassword);
            }
        });

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                  //check Internet Connexion
                boolean connected = false;
                ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                        connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
                    //we are connected to a network
                    connected = true;

                    utilisateur = Username.getEditText().getText().toString().trim();
                    mot_passe   = Password.getEditText().getText().toString().trim();


                    if (TextUtils.isEmpty(utilisateur)) {
                        Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (TextUtils.isEmpty(mot_passe)) {
                        Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (mot_passe.length() < 6) {
                        Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Auth();

                }
                else{
                    connected = false;

                    AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                    alertDialog.setTitle("Connexion failed !!");
                    alertDialog.setMessage("Check your connexion Please !");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }


///////////////////////////////////////////Cardinals////////////
                String email = String.valueOf(Username.getEditText().getText());
                String password =  String.valueOf(Password.getEditText().getText());


             /*   Credential credential = new Credential.Builder(utilisateur)
                        .setPassword(mot_passe)
                        .build();



                Log.e("FIRAAAAAAAAAAS", ""+credential.getAccountType());
                Log.e("boughzou", ""+utilisateur);


                if (TextUtils.isEmpty(email)||TextUtils.isEmpty(password)){
                    Toast.makeText(LoginActivity.this, "Encore Vide", Toast.LENGTH_LONG).show();
                }else {

                   // cardinalsPassword(credential);
                }
*/


///////////////////////////////////////////Cardinals////////////
               /* Credential credential = new Credential.Builder(utilisateur)
                        .setPassword(mot_passe)
                        .build();


                mCredentialsClient.save(credential).addOnCompleteListener(
                        new OnCompleteListener() {
                            @Override
                            public void onComplete(@NonNull Task task) {
                                if (task.isSuccessful()) {
                                    Log.d(TAG, "SAVE: OK");
                                    Toast.makeText(LoginActivity.this, "Credentials saved", Toast.LENGTH_SHORT).show();
                                    return;
                                }

                                Exception e = task.getException();
                                if (e instanceof ResolvableApiException) {
                                    // Try to resolve the save request. This will prompt the user if
                                    // the credential is new.
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    try {
                                        rae.startResolutionForResult(LoginActivity.this, RC_SAVE);
                                    } catch (IntentSender.SendIntentException el) {
                                        // Could not resolve the request
                                        Log.e(TAG, "Failed to send resolution.", el);
                                        Toast.makeText(LoginActivity.this, "Save failed", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    // Request has no resolution
                                    Toast.makeText(LoginActivity.this, "Save failed", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });*/

////////////////////////////////////////////////////////////////
            }
        });







    }

    @Override
    protected void onPause() {
        super.onPause();
        savePreferences();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadPreferences();
    }

    private void initializeControls(){
        ImageView imgLinkedin = (ImageView)findViewById(R.id.Llogo);
        imgLinkedin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                     //TODO linkedIn Connexion
            }
        });

    }

    private void savePreferences() {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        // Edit and commit
        UnameValue = Username.getEditText().getText().toString();
        PasswordValue = Password.getEditText().getText().toString();
        System.out.println("onPause save name: " + UnameValue);
        System.out.println("onPause save password: " + PasswordValue);
        editor.putString(PREF_UNAME, UnameValue);
        editor.putString(PREF_PASSWORD, PasswordValue);
        editor.commit();
    }
    private void loadPreferences() {

        SharedPreferences settings = getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        // Get value
        UnameValue = settings.getString(PREF_UNAME, DefaultUnameValue);
        PasswordValue = settings.getString(PREF_PASSWORD, DefaultPasswordValue);
        Username.getEditText().setText(UnameValue);
        Password.getEditText().setText(PasswordValue);
        System.out.println("onResume load name: " + UnameValue);
        System.out.println("onResume load password: " + PasswordValue);
    }

    private void Auth() {
        mAuth.signInWithEmailAndPassword(utilisateur, mot_passe)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {


                            String currentIDUser = mAuth.getCurrentUser().getUid();
                            //rootRef.child("users").child(currentIDUser).setValue("");



                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            final FirebaseUser user = mAuth.getCurrentUser();


                            connxionEtat = 0;
                            AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                            alertDialog.setTitle("successful connection !");
                            alertDialog.setMessage("Connected ");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent i = new Intent(LoginActivity.this, AccueilActivity.class);
                                            i.putExtra("etatconnexion", 0);
                                            i.putExtra("user",user);
                                            startActivity(i);
                                        }
                                    });
                            alertDialog.show();


                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());

                            AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                            alertDialog.setTitle("connection failed !!");
                            alertDialog.setMessage("check your email or password");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();

                        }


                    }
                });
             }



    private void LoginGoogle() {


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("406587408314-od6m1mhvsd7c40v4mj2oerb1uufi104q.apps.googleusercontent.com")
                .requestEmail()
                .build();
        final GoogleSignInClient  googleSignInClient = GoogleSignIn.getClient(this, gso);

        SignInButton googleButton = (SignInButton) findViewById(R.id.sign_in_button);
        googleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = googleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, 101);
            }
        });

        for (int i = 0; i < googleButton.getChildCount(); i++) {
            View v = googleButton.getChildAt(i);

            if (v instanceof TextView)
            {



                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);

                float yInches= metrics.heightPixels/metrics.ydpi;
                float xInches= metrics.widthPixels/metrics.xdpi;
                double diagonalInches = Math.sqrt(xInches*xInches + yInches*yInches);
                if (diagonalInches>=6.5){
                    // 6.5inch device or bigger

                    TextView tv = (TextView) v;

                    tv.setTypeface(null, Typeface.NORMAL);
                    tv.setTextColor(Color.parseColor("#FFFFFF"));
                    tv.setBackgroundDrawable(getResources().getDrawable(
                            R.drawable.google_icon_001));
                    tv.setSingleLine(true);
                    tv.setText("");

                    tv.setPadding(5, 5, 5, 5);

                    ViewGroup.LayoutParams params = tv.getLayoutParams();
                    params.width = 50;
                    params.height = 50;
                    tv.setLayoutParams(params);

                    return;
                }else{
                    // smaller device


                    TextView tv = (TextView) v;

                    tv.setTypeface(null, Typeface.NORMAL);
                    tv.setTextColor(Color.parseColor("#FFFFFF"));
                    tv.setBackgroundDrawable(getResources().getDrawable(
                            R.drawable.google_icon_001));
                    tv.setSingleLine(true);
                    tv.setText("");
                    tv.setPadding(5, 5, 5, 5);

                    ViewGroup.LayoutParams params = tv.getLayoutParams();
                    params.width = 45;
                    params.height = 45;
                    tv.setLayoutParams(params);

                    return;
                }

            }
        }

    }



    private void LoginFB() {

        FBLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fbLoginManager.logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "email"));
            }
        });


        fbLoginManager = com.facebook.login.LoginManager.getInstance();
        callbackManager = CallbackManager.Factory.create();


        fbLoginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                handleFacebookAccessToken(loginResult.getAccessToken());


                Intent intent = new Intent(LoginActivity.this, AccueilActivity.class);
                connxionEtat = 1;
                intent.putExtra("etatconnexion", connxionEtat);
                startActivity(intent);


            }

            @Override
            public void onCancel() {
                Toast.makeText(LoginActivity.this, "Login Facebook Canceled", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(LoginActivity.this, "Erreor !!!! "+error, Toast.LENGTH_LONG).show();
            }

        });
    }



    public  void  cardinalsPassword(Credential credential){


     /*   mCredentialsClient.save(credential).addOnCompleteListener(
                new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "SAVE: OK");
                            Toast.makeText(LoginActivity.this, "Credentials saved", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Exception e = task.getException();
                        if (e instanceof ResolvableApiException) {
                            // Try to resolve the save request. This will prompt the user if
                            // the credential is new.
                            ResolvableApiException rae = (ResolvableApiException) e;
                            try {
                                rae.startResolutionForResult(LoginActivity.this, RC_SAVE);
                            } catch (IntentSender.SendIntentException el) {
                                // Could not resolve the request
                                Log.e(TAG, "Failed to send resolution.", el);
                                Toast.makeText(LoginActivity.this, "Save failed", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            // Request has no resolution
                            Toast.makeText(LoginActivity.this, "Save failed", Toast.LENGTH_SHORT).show();
                        }
                        }
                    });*/



    }




    //OnActivity results facebook and google integrated
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
       callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);



        ///////////////////// ccardinals//////////////////////
        if (requestCode == RC_SAVE) {
            if (resultCode == RESULT_OK) {
                Log.d(TAG, "SAVE: OK");
                Toast.makeText(this, "Credentials saved", Toast.LENGTH_SHORT).show();
            } else {
                Log.e(TAG, "SAVE: Canceled by user");
            }
        }
        ///////////////////////////////////////////////////////




        if (resultCode == Activity.RESULT_OK)

            switch (requestCode) {

                case 101:
                    try {
                        // The Task returned from this call is always completed, no need to attach
                        // a listener.
                        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                        GoogleSignInAccount account = task.getResult(ApiException.class);
                        firebaseAuthWithGoogle(account);


                        Intent intentGoogle = new Intent(LoginActivity.this, AccueilActivity.class);
                        connxionEtat = 1;
                        intentGoogle.putExtra("etatconnexion",connxionEtat);
                        startActivity(intentGoogle);

                    } catch (ApiException e) {
                        // The ApiException status code indicates the detailed failure reason.
                        Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
                    }
                    break;
            }
    }



    //link facebook compt to firebase
    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {


                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();


                            String currentIDUser = mAuth.getCurrentUser().getUid();
                            System.out.println("dhia"+ currentIDUser);

                            //rootRef.child("users").child(currentIDUser).setValue("");

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    //link Google compt to firebase
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());


        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();



                            String currentIDUser = mAuth.getCurrentUser().getUid();
                            System.out.println("dhia"+ currentIDUser);

                            //rootRef.child("users").child(currentIDUser).setValue("");


                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();

                        }

                    }
                });
    }

}
