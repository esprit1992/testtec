package com.intensy_t.pinowl.Interfaces;

import com.google.gson.JsonObject;
import com.intensy_t.pinowl.Entities.Events;
import com.intensy_t.pinowl.Entities.User;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface JsonPlaceHolerAPI {

    @FormUrlEncoded
    @POST("users")
    Call<ResponseBody> addUser(
            @Field("role") String role,
            @Field("username") String username,
            @Field("email")String email,
            @Field("fullname") String fullname,
            @Field("imgUser") String imgUser,
            @Field("gender") String gender,
            @Field("age") String age
    );

    @FormUrlEncoded
    @PUT("/users/_id")
    Call<ResponseBody> editUser(
            @Field("role") String role,
            @Field("username") String username,
            @Field("email")String email,
            @Field("fullname") String fullname,
            @Field("imgUser") String imgUser,
            @Field("gender") String gender,
            @Field("age") String age
    );

    @GET("events")
    Call<List<Events>> getEvents();

    @GET("users")
    Call<List<User>> getUsers();

    @GET("events")
    Call<List<Events>> getProf_Events();

    @GET("email/{mail}")
    Call<JsonObject> getProf_User(@Path("mail") String mail);

}
