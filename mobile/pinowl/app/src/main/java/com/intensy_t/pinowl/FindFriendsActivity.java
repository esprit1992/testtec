package com.intensy_t.pinowl;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.intensy_t.pinowl.Entities.Contact;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class FindFriendsActivity extends AppCompatActivity {


    private Toolbar mToolbar;
    private RecyclerView findFreindRecycle;


    private DatabaseReference userRef;
    FirebaseDatabase fireData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friends);

        findFreindRecycle = findViewById(R.id.find_friend_recycler);
        findFreindRecycle.setLayoutManager( new LinearLayoutManager(this  ));



        userRef = FirebaseDatabase.getInstance().getReference().child("users");
        fireData = FirebaseDatabase.getInstance();


        mToolbar = findViewById(R.id.find_friends_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Find Friends");

    }


    @Override
    protected void onStart() {
        super.onStart();



        FirebaseRecyclerOptions <Contact> options = new FirebaseRecyclerOptions.Builder<Contact>()
                .setQuery(userRef, Contact.class )
                .build();


        FirebaseRecyclerAdapter<Contact,FindFriendViewHoler> adapter =
                new FirebaseRecyclerAdapter<Contact, FindFriendViewHoler>(options) {


            @Override
            protected void onBindViewHolder(@NonNull FindFriendViewHoler holder, final int position, @NonNull Contact model) {
                holder.FriendName.setText(model.getName());
                holder.FriendRole.setText(model.getRole());

                Picasso.get().load(model.getImage()).placeholder(R.drawable.avatar_pic_001).into(holder.FriendImage);

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String visit_user_id =  getRef(position).getKey();
                        Intent profilIntent = new Intent(FindFriendsActivity.this,Visit_ProfilActivity.class);
                        profilIntent.putExtra("visit_user_id", visit_user_id);
                        startActivity(profilIntent);
                    }
                });
            }
            @NonNull
            @Override
            public FindFriendViewHoler onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

                    View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_find_friends, viewGroup,false);
                    FindFriendViewHoler viewHoler = new FindFriendViewHoler(view);
                    return viewHoler;
            }
        };

        findFreindRecycle.setAdapter(adapter);
        adapter.startListening();

    }

    public static class FindFriendViewHoler extends  RecyclerView.ViewHolder{
        TextView FriendName,FriendRole;
        CircleImageView FriendImage;

        public FindFriendViewHoler(@NonNull View itemView) {
            super(itemView);

            FriendName  = itemView.findViewById(R.id.friend_profil_name);
            FriendRole  = itemView.findViewById(R.id.friend_role);
            FriendImage = itemView.findViewById(R.id.friend_img);
        }
    }
}
