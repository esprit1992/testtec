'use strict';
const User = require('../models/user');
exports.getUsers = function (req, res, next) {
    User.getUsers((err, users) => {
		if (err) {
			throw err;
		}
		res.json(users);
	});
}
exports.getUserById = function (req, res, next) {
    User.getUserById(req.params._id, (err, user) => {
		if (err) {
			throw err;
		}
		res.json(user);
	});
}
exports.getUserByEmail = function (req,res,next){
    User.getUserByMail(req.params.mail, (err, user) => {
		if (err) {
			throw err;
		}
		res.json(user);
	});
}
exports.addUser = function (req, res, next) {
    var user = req.body;
	User.addUser(user, (err, user) => {
		if (err) {
			throw err;
		}
		res.json(user);
	});
}
exports.updateUser = function (req, res, next) {
    var id = req.params._id;
	var user = req.body;
	User.updateUser(id, user, {}, (err, user) => {
		if (err) {
			throw err;
		}
		res.json(user);
	});
}
exports.removeUSer = function (req, res, next) {
    var id = req.params._id;
	User.removeUser(id, (err, user) => {
		if (err) {
			throw err;
		}
		res.json(user);
	});
}
