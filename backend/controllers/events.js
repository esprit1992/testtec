'use strict';
const Event = require('../models/event');
const sponsor = require('../models/sponsor');
exports.getEvents = function (req, res, next) {
	/*sponsor.addSponsor({"name":"testSP"},function(r){
		console.log(t);
	})
	sponsor.find().exec().then(tt=>{
		console.log(tt)
	})*/
    Event.find()
        //.select('name creator sponsor')
		.populate('creator')
		.populate('sponsor')
        .exec()
        .then(events => {
            return res.json(events);

        })
        .catch(err => {
            console.log(err);
        });
    /*Event.getEvents((err, events) => {
		if (err) {
			throw err;
		}
		res.json(events);
	});*/
}
exports.getEventById = function (req, res, next) {
    Event.getEventById(req.params._id, (err, event) => {
        if (err) {
            throw err;
        }
        res.json(event);
    });
}
exports.addEvent = function (req, res, next) {
	var event = req.body;
	Event.addEvent(event, (err, event) => {
		if (err) {
			throw err;
		}
		res.json(event);
	});
}
exports.updateEvent = function (req, res, next) {
    var id = req.params._id;
	var event = req.body;
	Event.updateEvent(id, event, {}, (err, event) => {
		if (err) {
			throw err;
		}
		res.json(event);
	});
}
exports.removeEvent = function (req, res, next) {
    var id = req.params._id;
	Event.removeEvent(id, (err, event) => {
		if (err) {
			throw err;
		}
		res.json(event);
	});
}
