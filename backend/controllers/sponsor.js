'use strict';
const sponsor = require('../models/sponsor');
exports.getSponsor = function (req, res, next) {
    sponsor.getSponsors((err, sponsor) => {
		if (err) {
			throw err;
		}
		return res.json(sponsor);
	});
}