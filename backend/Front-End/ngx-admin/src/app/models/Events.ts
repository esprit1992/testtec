import { User } from "./Users";
import { Sponsor } from "./Sponsor";

export class Event {
    _id?: String;
    name?: string="";
    imgEvent?: string="";
    creator?: User=new User();
    description?: string="";
    venu?: [User];
    sponsor?:[Sponsor];
    create_date?:Date=new Date()
    date?:Date=new Date();
}
