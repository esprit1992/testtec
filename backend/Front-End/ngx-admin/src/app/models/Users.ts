export class User {
    _id?: String;
    role?: string="";
    username?: string="";
    email?: string="";
    fullname?: string="";
    imgUser?: string="";
    gender:boolean=true;
    age?:string;
}
