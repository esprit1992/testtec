import { Component, OnInit, NgZone, OnDestroy, Input, EventEmitter, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from '../../../models/Users';
import { MyUserService } from '../../../@core/services/user.service';
import { Event } from '../../../models/Events';
import { EventService } from '../../../@core/services/event.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';



@Component({
  selector: 'ngx-show-remove-user',
  templateUrl: './selectUser.component.html',
  styleUrls: ['./selectUser.component.scss'],
})

export class selectuserComponet implements OnInit {
  @Output() updatedone: EventEmitter<any> = new EventEmitter<any>();
  @Input() event: Event;
  users: User[] = [];
  usertoshow: User[] = [];
  numbers = [] = [];
  filter = "";
  constructor(protected service: MyUserService,
    private activeModal: NgbActiveModal,
    protected eventService: EventService) { }
  updateuser(user) {
    if (!this.event.venu)
      this.event.venu = [user];
    else {
      let index = this.event.venu.indexOf(user);
      if (index > -1)
        this.event.venu.splice(index, 1);
      else
        this.event.venu.push(user)
    }
  }
  ngOnInit() {
    this.service.getAll().subscribe((result: any) => {
      this.users = result;
      this.Myfilter();
    });
  }

  Myfilter() {
    this.usertoshow = this.users.filter(
      user => { return user.username.toLowerCase().includes(this.filter.toLowerCase()) });
    this.numbers = Array(Math.ceil(this.usertoshow.length / 3)).fill(0).map((x, i) => i);
  }
  saveChange() {
    this.eventService.update(this.event).subscribe(data => {
      this.updatedone.emit(this.event);
      this.activeModal.close();
    });
  }
}

