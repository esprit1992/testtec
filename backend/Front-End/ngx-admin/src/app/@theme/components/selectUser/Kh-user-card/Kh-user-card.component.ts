import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../../../models/Users';


@Component({
  selector: 'Kh-user-card',
  templateUrl: './Kh-user-card.component.html',
  styleUrls: ['./Kh-user-card.component.scss'],
})
export class KhUserCardComponent {
  @Input() user: User;
  @Input() isSelected:boolean=false;
  @Output() updateuser: EventEmitter<any> = new EventEmitter<any>()
  state="info";
  enable="success";
  accstatus="";
  constructor() { 
    
  }

  addremoveclicked(){
    this.isSelected = !this.isSelected;
    this.updateuser.emit(this.user._id);
  }

}
