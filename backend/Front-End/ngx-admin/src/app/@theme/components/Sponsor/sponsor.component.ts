import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { SponsorService } from "../../../@core/services/sponsor.service";
import { Sponsor } from "../../../models/Sponsor";

@Component({
  selector: 'SponsorAll',
  styleUrls: ['./sponsor.component.scss'],
  templateUrl: './sponsor.component.html',
})

//its a comonet use to edit and add a parking object its the same used for add and
export class SponsorSelectComponent implements OnInit {
  sponsorList: Sponsor[] = [];
  @Input() sponsorOfThisEvent: Sponsor[] = []
  @Output() sponsorListChanged: EventEmitter<any> = new EventEmitter<any>();
  constructor(private sponsorService: SponsorService) {

  }
  ngOnInit(): void {
    this.sponsorService.getAll().subscribe((result: any[]) => {
      this.sponsorList = result;
    });
  }
  sponsorChanged(change) {
    console.log(change);
    console.log(this.sponsorOfThisEvent[0]);
    if (this.sponsorOfThisEvent) {
      let index = this.sponsorOfThisEvent.indexOf(this.sponsorOfThisEvent.find(e=>e._id==change._id));
      if (index > -1)
        this.sponsorOfThisEvent.splice(index, 1);
      else
        this.sponsorOfThisEvent.push(change)
      this.sponsorListChanged.emit(this.sponsorOfThisEvent);
    }
  }
}