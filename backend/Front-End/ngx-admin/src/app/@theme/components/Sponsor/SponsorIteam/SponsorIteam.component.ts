import { Component, Input, EventEmitter, Output, OnInit, OnChanges, SimpleChanges } from "@angular/core";
import {  Sponsor } from "../../../../models/Sponsor";


@Component({
    selector: 'SponsorOne',
    styleUrls: ['./SponsorIteam.component.scss'],
    templateUrl: './SponsorIteam.component.html',
  })
  
  //its a comonet use to edit and add a parking object its the same used for add and
  export class SponsorIteamComponent  implements OnChanges{
  ngOnChanges(changes: SimpleChanges): void {
    if(changes.sponsorthisevent && this.sponsorthisevent){  
      let element = this.sponsorthisevent.find(iteam=>iteam._id==this.iteam._id);
    if(element)
        this.selected=true;
      else
        this.selected=false;
  }
}
    @Input() type: Number = 0;// 0  use to add/remove | 1  use to show 
    @Output() sponsorChanged: EventEmitter<any> = new EventEmitter<any>();
    @Input() iteam: Sponsor;
    @Input() sponsorthisevent:Sponsor[]=[]
    selected:boolean=false;
    
    clicked(){
      if(this.type==0){
        this.selected= !this.selected;
        this.sponsorChanged.emit(this.iteam);
      }
    }
    
  }