import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ViewCell } from "ng2-smart-table";

@Component({
  selector: 'button-view',
  template: `
    <div>
    <nb-actions size="small" inverse = "false" fullWidth="true">        
                                <nb-action (click)="onClick()"  [badgeText]="rowData.venu?rowData.venu.length+' users':'0 user'"
                                badgePosition="top right"
                                badgeStatus="info" icon="fa fa-user-edit"></nb-action>
                              </nb-actions>
  `,
})
export class AddUserToEventComponent implements ViewCell {
  renderValue: string;

  @Input() value: string | number;
  @Input() rowData: any;

  @Output() addUserClicked: EventEmitter<any> = new EventEmitter();



  onClick() {
    this.addUserClicked.emit(this.rowData);
  }
}