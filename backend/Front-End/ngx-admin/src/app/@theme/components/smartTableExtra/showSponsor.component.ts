import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ViewCell } from "ng2-smart-table";

@Component({
  selector: 'showSponsor',
  styles:['SponsorContainer { overflow: auto;white-space: nowrap;margin: auto;display: list-item; }','iteam{display: inline-block;}'],
  template: `
  <div class="row SponsorContainer">
  <SponsorOne (sponsorChanged)="sponsorChanged($event)" *ngFor="let item of rowData.sponsor" class="iteam" [type]='1'  [iteam]="item"></SponsorOne>
  </div>
  `,
})
export class showSponsor implements ViewCell ,OnInit {
  ngOnInit(): void {
    console.log("iam here");
  }
  @Input() value: string | number;
  @Input() rowData: any;
}
