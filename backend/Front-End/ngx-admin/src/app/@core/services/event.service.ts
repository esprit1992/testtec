import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Observable } from "rxjs/Observable";
import { Event } from '../../models/Events';






@Injectable()
export class EventService {
  private apiUrl = 'http://localhost:8080/api/events/';
  
  constructor(private http: HttpClient) {

  }
 
  

  getAll(): Observable<any[]>  {
    return this.http.get(this.apiUrl)
      .map((response) => {
        if (response) {
          return response;
        }
    }).catch((error: any) => {
        if (error.status === 422|| error.status === 404 ) {
            return [error]
        }
        else return[{"status":502,error:{"codeAPP":499}}]
    });
  }

 
 
 
  add(event: Event): Observable<any> {
    return this.http
    .post(this.apiUrl,event)
      .map((response) => {
        if (response) {
          return response;
        }
    }).catch((error: any) => {
      if (error.status === 422 ) {
        return [error]
    }
    else return[{"status":502,error:{"codeAPP":499}}]
    }); 
  }
 
  delete(event: Event): Observable<any> {
    return this.http
    .delete(this.apiUrl+event._id)
      .map((response) => {
        if (response) {
          return response;
        }
    }).catch((error: any) => {
      if (error.status === 422 ) {
        return [error]
    }
    else return[{"status":502,error:{"codeAPP":499}}]
    }); 
  }
 
  update(event: Event): Observable<any> {
    return this.http
    .put(this.apiUrl+event._id,event)
      .map((response) => {
        if (response) {
          return response;
        }
    }).catch((error: any) => {
      if (error.status === 422 ) {
        return [error]
    }
    else return[{"status":502,error:{"codeAPP":499}}]
    });
  }
}