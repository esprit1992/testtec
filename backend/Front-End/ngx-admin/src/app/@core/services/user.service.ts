import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Observable } from "rxjs/Observable";







@Injectable()
export class MyUserService {
  private apiUrl = 'http://localhost:8080/api/users/';

  constructor(private http: HttpClient) {

  }



  getAll(): Observable<any> {
    return this.http.get(this.apiUrl).map((response) => {
      if (response) {
        return response;
      }
    }).catch((error: any) => {
      if (error.status === 422) {
        return [error]
      }
      else return [{ "status": 502, error: { "codeAPP": 499 } }]
    });
  }
}