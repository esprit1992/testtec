import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  
 
  {
    title: 'Event',
    icon: 'nb-star',
    children: [
      {
        title: 'Add New Event',
        link: '/pages/event/new',
      },
      {
        title: 'Show/Remove Event',
        link: '/pages/event/show',
      },
    ],
  },
];
