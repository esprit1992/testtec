import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { showRemoveEventsComponent, addUpdateEventComponent,  } from '.';
import { eventComponent } from './Event.Component';




const routes: Routes = [{
  path: '',
  component: eventComponent,
  children: [
    {
      path: '',
      component: eventComponent,
    },
    {
      path: 'new',
      component: addUpdateEventComponent,
    },
    {
      path: 'show',
      component: showRemoveEventsComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParkingRoutingModule { }

export const routedComponents = [
  eventComponent,
  
];

