import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParkingRoutingModule } from './event-routing.module';
import { showRemoveEventsComponent,addUpdateEventComponent } from '.';
import { ThemeModule } from '../@theme/theme.module';

import { ReactiveFormsModule } from '@angular/forms';
import { eventComponent } from './Event.Component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TablesModule } from '../pages/tables/tables.module';


const COMPONENTS = [
    eventComponent,
showRemoveEventsComponent,
  addUpdateEventComponent,
 ];

@NgModule({
  imports: [
    CommonModule,TablesModule,ParkingRoutingModule,ThemeModule,ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'})
  ],
  entryComponents:[addUpdateEventComponent],
  declarations:[COMPONENTS],
  exports:[COMPONENTS],
  providers:[NgbActiveModal
    ]
})
export class eventModule { }
