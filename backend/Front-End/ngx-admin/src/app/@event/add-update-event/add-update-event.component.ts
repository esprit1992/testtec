import { Component, ViewChild, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EventService } from '../../@core/services/event.service';
import { MyUserService } from '../../@core/services/user.service';
import { Event } from '../../models/Events';
//import { ToastComponent } from '../../@theme/components/Toast/Toast.component';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { NbAuthService, NbAuthJWTToken } from '@nebular/auth';
import { User } from '../../models/Users';
import { NbDateService } from '@nebular/theme';






@Component({
  selector: 'Ngx-add-update-event',
  styleUrls: ['./add-update-event.component.scss'],
  templateUrl: './add-update-event.component.html',
})

//its a comonet use to edit and add a parking object its the same used for add and
export class addUpdateEventComponent implements OnInit {
  @Input() type: Number = 0;// 0 form use to add | 1 form use to update
  @Input() event: Event;
  @Output() updatedone: EventEmitter<any> = new EventEmitter<any>()
  submitted: boolean;
  eventForm: FormGroup;
  users: User[]=[]
  controlForm: any;
  ngOnInit(): void {
    this.UserService.getAll().subscribe((result: any[]) => {
      this.users = result;
    }
    );

    setTimeout(() => {
      this.init();
    }, 100);
  }
  constructor(
    protected UserService:MyUserService,
    protected dateService: NbDateService<Date>,
    protected EventService: EventService,
    protected router: Router,
    // private toast: ToastComponent,
    private activeModal: NgbActiveModal,
    private fb: FormBuilder) {

    this.createForm();


  }

  setFormDetails() {
    if (this.event) {
      this.eventForm.patchValue({
        name: this.event.name,
        //imgEvent: this.event.imgEvent,
        creator: this.event.creator._id,
        description: this.event.description,
        date: new Date(new Date(this.event.date).setDate(new Date(this.event.date).getDate()-1)),
        sponsor: this.event.sponsor
      })
    }
  }
  init() {

    this.submitted = false;
    if (this.type == 1) {
      this.setFormDetails();
    } else
      this.event = new Event();
  }


  prepareParkingInfo() {
    
    const formModel = this.eventForm.value;
    this.event.name = formModel.name as string;
    this.event.creator=this.users.find(iteam=>iteam._id==formModel.creator) ;
    this.event.description = formModel.description as string;
    this.event.date =  new Date(formModel.date.setDate(formModel.date.getDate() + 1)) as Date
  }
  submit(): void {
    if (!this.submitted) {
      this.prepareParkingInfo();
      this.submitted = true;
      this.eventForm.disable();
      
      if (this.type == 0) {
        this.EventService.add(this.event).subscribe(data => {
          this.submitted = false;
          this.eventForm.enable();
          this.eventForm.reset();
          this.init();
        });
      } else {
        console.log(this.event)
        this.EventService.update(this.event).subscribe(data => {
         
          this.updatedone.emit(this.event);
          this.activeModal.close();
        });
      }
    }
  }

  createForm() {
    this.eventForm = this.fb.group({
      //Validators.compose([Validators.required, Validators.minLength(this.controlForm.minNameValue), Validators.maxLength(this.controlForm.maxNamedValue)])
      name: [''],
      imgEvent: [''],
      creator: [''],
      description: [''],
      date: [''],
      sponsor: [''],
    });

  }

  onOwnerChangeChange(ownerId) {
    this.event.creator._id = ownerId;
  }
  //Image Event
  handleInputChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];

    var pattern = /image-*/;
    var reader = new FileReader();

    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleReaderLoaded(e) {
    var reader = e.target;

    this.event.imgEvent = reader.result.split("base64,")[1];

  }
  sponsorListChanged(sponserList){
    this.event.sponsor=sponserList;
  }

}


