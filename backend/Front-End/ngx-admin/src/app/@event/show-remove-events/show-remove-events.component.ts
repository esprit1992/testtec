import { Component, ViewChild, OnDestroy, OnInit, NgZone } from '@angular/core';
import { EventService } from '../../@core/services/event.service';
//import { ToastComponent } from '../../@theme/components/Toast/Toast.component';
import { addUpdateEventComponent } from '../add-update-event/add-update-event.component';
import { Subscription, from } from 'rxjs';
import { LocalDataSource } from 'ng2-smart-table';
import { Event } from '../../models/Events';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddUserToEventComponent, selectuserComponet } from '../../@theme/components';
import { showSponsor } from '../../@theme/components/smartTableExtra/showSponsor.component';



@Component({
  selector: 'ngx-show-remove-event',
  styleUrls: ['./show-remove-events.component.scss'],
  templateUrl: './show-remove-events.component.html',
})

 

export class showRemoveEventsComponent implements OnInit {
  ngOnInit(): void {
  }
  source: LocalDataSource;
  selectedEvent:Event = new Event();
  settings ={
    mode: "external",
    noDataMessage: "",
    actions: {
      add: false,
      columnTitle: '',
    },
    pager: {
      display: true,
      perPage: 5,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    columns: {
      name: {
        title: "Name",
        type: 'text',
      },
      creatorname: {
        title: "creator",
        type: 'text',
      },
      description: {
        title: "Description",
        type: 'text'
      },
      date: {
        title: "Date",
        type: 'text',
      },
      sponsorSize: {
        filter: false,
        sort:false,
        title: 'Sponsor',
        type: 'custom',
        renderComponent: showSponsor,
      },
      button: {
        filter: false,
        sort:false,
        title: 'Invited Users',
        type: 'custom',
        renderComponent: AddUserToEventComponent,
        onComponentInitFunction: (instance: any) => {
          instance.addUserClicked.subscribe(row => {
          this.openModalToSelectUser(row);
          });
        },
      },
    },
  }
  constructor(private service: EventService,
    //private toast: ToastComponent,
    private ngZone: NgZone,
    private modalService: NgbModal,
 ) {
    this.source = new LocalDataSource();
    this.service.getAll().subscribe((result: any[]) => {
        result.forEach(element => {
          try {
            element["creatorname"] = element["creator"].username;
            element["sponsorSize"]=element["sponsor"].length;
          } catch (error) { console.log(error); }
        });
        this.source.load(result);
      
    });
  }
 

  openModalToSelectUser(event){
    let editInstance = this.modalService.open(selectuserComponet, { size: 'lg', container: 'nb-layout' }).componentInstance;
    editInstance.event = event;

    editInstance.updatedone.subscribe((Newevent) => {
      this.source.getElements().then((dataset)=>{
        let myXiteam=dataset.find(iteam=>iteam._id==event._id);
          this.source.update(myXiteam,Newevent).then().catch(e=>{console.log(e)});
      }).catch(e=>{console.log(e)})
    
  });
  }

  onDelete(event): void {
    if (window.confirm("delete confirmation")) {
      this.service.delete(event.data).subscribe(data => {
       console.log("removed");
       this.source.remove(event.data);
      });
    }
  }

  onEdit(event): void {
    let editInstance = this.modalService.open(addUpdateEventComponent, { size: 'lg', container: 'nb-layout' }).componentInstance;
    editInstance.type = 1;
    editInstance.event = JSON.parse(JSON.stringify(event.data));
    editInstance.updatedone.subscribe((Newevent) => {
      Newevent["creatorname"] = Newevent["creator"].username;
      Newevent["sponsorSize"]=Newevent["sponsor"].length;
      Newevent["date"]=new Date(Newevent["date"]).toISOString();
      this.source.getElements().then((dataset)=>{
        let myXiteam=dataset.find(iteam=>iteam._id==event.data._id);
          this.source.update(myXiteam,Newevent).then().catch(e=>{console.log(e)});
      }).catch(e=>{console.log(e)})
    
  });
}
}