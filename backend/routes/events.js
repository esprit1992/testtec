const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();
const events = require('../controllers/events');
const jsonParser = bodyParser.json({limit: '50mb'});


router.get('/', events.getEvents);

router.get('/:_id', events.getEventById);

router.post('/',jsonParser, events.addEvent);

router.put('/:_id',jsonParser, events.updateEvent);

router.delete('/:_id', events.removeEvent);



module.exports = router;