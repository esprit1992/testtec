const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();
const users = require('../controllers/users');
const jsonParser = bodyParser.json();

router.get('/', users.getUsers);

router.get('/:_id', users.getUserById);

router.post('/',jsonParser, users.addUser);

router.put('/:_id',jsonParser, users.updateUser);

router.delete('/:_id', users.removeUSer);

router.get('/email/:mail',users.getUserByEmail)

module.exports = router;