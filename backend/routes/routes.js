'use strict';
const users = require('./users');
const events = require('./events');
const sponsor = require('./sponsor');
const cors = require('cors');
var corsOptions = {
    origin: '*'
  };
module.exports = function (app) {
    app.use(cors(corsOptions));
    app.use('/api/users',users);  
    app.use('/api/events',events);
    app.use('/api/sponsor',sponsor);
};