'use strict';
require('dotenv').config({path: './config/.env'});
const express = require('express');
const app = express();
var http = require('http');
const morgan = require('morgan');
const mongoose = require('mongoose');
var server = require('http').Server(app); 
app.use(morgan('combined'));


mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true }, function (error, db) {
    if (error) 
    throw new Error('Database failed to connect!');
    else{
    console.log('MongoDB successfully connected');
    exports.db = db;
    const routes = require('./routes/routes.js');
    routes(app);
    app.get('/', (req, res) => {
		res.send('Please use /api/events or /api/users');
	});
    // start the server
    server.listen((process.env.PORTAPPLICATIONLOCAL || 8080), function() {
        console.log('Express server listening on port', (process.env.PORTAPPLICATIONLOCAL || 8080));
        
    });
}
});











/*************************************************
Invites
**************************************************/
/*
app.get('/api/invites', (req, res) => {
	Invite.getInvites((err, invite) => {
		if (err) {
			throw err;
		}
		res.json(invite);
	});
});

app.get('/api/invites/:_id', (req, res) => {
	Invite.getInviteById(req.params._id, (err, invite) => {
		if (err) {
			throw err;
		}
		res.json(invite);
	});
});

app.post('/api/invites', (req, res) => {
	var invite = req.body;
	Invite.addInvite(invite, (err, invite) => {
		if (err) {
			throw err;
		}
		res.json(invite);
	});
});

app.put('/api/invites/:_id', (req, res) => {
	var id = req.params._id;
	var invite = req.body;
	Invite.updateInvite(id, invite, {}, (err, invite) => {
		if (err) {
			throw err;
		}
		res.json(invite);
	});
});

app.delete('/api/invites/:_id', (req, res) => {
	var id = req.params._id;
	Invite.removeInvite(id, (err, invite) => {
		if (err) {
			throw err;
		}
		res.json(invite);
	});
});
*/
/******************************************
Attend
******************************************/
/*
app.get('/api/attends', (req, res) => {
	Attend.getAttends((err, attend) => {
		if (err) {
			throw err;
		}
		res.json(attend);
	});
});

app.get('/api/attends/:_id', (req, res) => {
	Attend.getAttendById(req.params._id, (err, attend) => {
		if (err) {
			throw err;
		}
		res.json(attend);
	});
});

app.post('/api/attends', (req, res) => {
	var attend = req.body;
	Attend.addAttend(attend, (err, attend) => {
		if (err) {
			throw err;
		}
		res.json(attend);
	});
});

app.put('/api/attends/:_id', (req, res) => {
	var id = req.params._id;
	var attend = req.body;
	Attend.updateAttend(id, attend, {}, (err, attend) => {
		if (err) {
			throw err;
		}
		res.json(attend);
	});
});

app.delete('/api/attends/:_id', (req, res) => {
	var id = req.params._id;
	Attend.removeAttend(id, (err, attend) => {
		if (err) {
			throw err;
		}
		res.json(attend);
	});
});
*/
/*************************************************************
Accompany
**************************************************************/
/*
app.get('/api/accompanys', (req, res) => {
	Accompany.getAccompanys((err, accompany) => {
		if (err) {
			throw err;
		}
		res.json(accompany);
	});
});

app.get('/api/accompanys/:_id', (req, res) => {
	Accompany.getAccompanyById(req.params._id, (err, accompany) => {
		if (err) {
			throw err;
		}
		res.json(accompany);
	});
});

app.post('/api/accompanys', (req, res) => {
	var accompany = req.body;
	Accompany.addAccompany(accompany, (err, accompany) => {
		if (err) {
			throw err;
		}
		res.json(accompany);
	});
});

app.put('/api/accompanys/:_id', (req, res) => {
	var id = req.params._id;
	var accompany = req.body;
	Accompany.updateAccompany(id, accompany, {}, (err, accompany) => {
		if (err) {
			throw err;
		}
		res.json(accompany);
	});
});

app.delete('/api/accompanys/:_id', (req, res) => {
	var id = req.params._id;
	Accompany.removeAccompany(id, (err, accompany) => {
		if (err) {
			throw err;
		}
		res.json(accompany);
	});
});
*/
/***********************************************
Sponsors
***********************************************/
/*
app.get('/api/sponsors', (req, res) => {
	Sponsor.getSponsors((err, sponsor) => {
		if (err) {
			throw err;
		}
		res.json(sponsor);
	});
});

app.get('/api/sponsors/:_id', (req, res) => {
	Sponsor.getSponsorById(req.params._id, (err, sponsor) => {
		if (err) {
			throw err;
		}
		res.json(sponsor);
	});
});

app.post('/api/sponsors', (req, res) => {
	var sponsor = req.body;
	Sponsor.addSponsor(sponsor, (err, sponsor) => {
		if (err) {
			throw err;
		}
		res.json(sponsor);
	});
});

app.put('/api/sponsors/:_id', (req, res) => {
	var id = req.params._id;
	var sponsor = req.body;
	Sponsor.updateSponsor(id, sponsor, {}, (err, sponsor) => {
		if (err) {
			throw err;
		}
		res.json(sponsor);
	});
});

app.delete('/api/sponsors/:_id', (req, res) => {
	var id = req.params._id;
	Sponsor.removeSponsor(id, (err, sponsor) => {
		if (err) {
			throw err;
		}
		res.json(sponsor);
	});
});
*/
/*************************************************
Sponsoring
**************************************************/
/*
app.get('/api/sponsorings', (req, res) => {
	Sponsoring.getSponsorings((err, sponsoring) => {
		if (err) {
			throw err;
		}
		res.json(sponsoring);
	});
});

app.get('/api/sponsorings/:_id', (req, res) => {
	Sponsoring.getSponsoringById(req.params._id, (err, sponsoring) => {
		if (err) {
			throw err;
		}
		res.json(sponsoring);
	});
});

app.post('/api/sponsorings', (req, res) => {
	var sponsoring = req.body;
	Sponsoring.addSponsoring(sponsoring, (err, sponsoring) => {
		if (err) {
			throw err;
		}
		res.json(sponsoring);
	});
});

app.put('/api/sponsorings/:_id', (req, res) => {
	var id = req.params._id;
	var sponsoring = req.body;
	Sponsoring.updateSponsoring(id, sponsoring, {}, (err, sponsoring) => {
		if (err) {
			throw err;
		}
		res.json(sponsoring);
	});
});

app.delete('/api/sponsorings/:_id', (req, res) => {
	var id = req.params._id;
	Sponsoring.removeSponsoring(id, (err, sponsoring) => {
		if (err) {
			throw err;
		}
		res.json(sponsoring);
	});
});
*/
/*********************************************************
Cathegory
**********************************************************/
/*
app.get('/api/cathegorys', (req, res) => {
	Cathegory.getCathegorys((err, cathegory) => {
		if (err) {
			throw err;
		}
		res.json(cathegory);
	});
});

app.get('/api/cathegorys/:_id', (req, res) => {
	Cathegory.getCathegoryById(req.params._id, (err, cathegory) => {
		if (err) {
			throw err;
		}
		res.json(cathegory);
	});
});

app.post('/api/cathegorys', (req, res) => {
	var cathegory = req.body;
	Cathegory.addCathegory(cathegory, (err, cathegory) => {
		if (err) {
			throw err;
		}
		res.json(cathegory);
	});
});

app.put('/api/cathegorys/:_id', (req, res) => {
	var id = req.params._id;
	var cathegory = req.body;
	Cathegory.updateCathegory(id, cathegory, {}, (err, cathegory) => {
		if (err) {
			throw err;
		}
		res.json(cathegory);
	});
});

app.delete('/api/cathegorys/:_id', (req, res) => {
	var id = req.params._id;
	Cathegory.removeCathegory(id, (err, cathegory) => {
		if (err) {
			throw err;
		}
		res.json(cathegory);
	});
});
*/
/*********************************************************
Organizer
**********************************************************/
/*
app.get('/api/organizers', (req, res) => {
	Organizer.getOrganizers((err, organizer) => {
		if (err) {
			throw err;
		}
		res.json(organizer);
	});
});

app.get('/api/organizers/:_id', (req, res) => {
	Organizer.getOrganizerById(req.params._id, (err, organizer) => {
		if (err) {
			throw err;
		}
		res.json(organizer);
	});
});

app.post('/api/organizers', (req, res) => {
	var organizer = req.body;
	Organizer.addOrganizer(organizer, (err, organizer) => {
		if (err) {
			throw err;
		}
		res.json(organizer);
	});
});

app.put('/api/organizers/:_id', (req, res) => {
	var id = req.params._id;
	var organizer = req.body;
	Organizer.updateOrganizer(id, organizer, {}, (err, organizer) => {
		if (err) {
			throw err;
		}
		res.json(organizer);
	});
});

app.delete('/api/organizers/:_id', (req, res) => {
	var id = req.params._id;
	Organizer.removeOrganizer(id, (err, organizer) => {
		if (err) {
			throw err;
		}
		res.json(organizer);
	});
});
*/
/*********************************************************
Organizing(realtional table)
**********************************************************/
/*
app.get('/api/organizings', (req, res) => {
	Organizing.getOrganizings((err, organizing) => {
		if (err) {
			throw err;
		}
		res.json(organizing);
	});
});

app.get('/api/organizings/:_id', (req, res) => {
	Organizing.getOrganizingById(req.params._id, (err, organizing) => {
		if (err) {
			throw err;
		}
		res.json(organizing);
	});
});

app.post('/api/organizings', (req, res) => {
	var organizing = req.body;
	Organizing.addOrganizing(organizing, (err, organizing) => {
		if (err) {
			throw err;
		}
		res.json(organizing);
	});
});

app.put('/api/organizings/:_id', (req, res) => {
	var id = req.params._id;
	var organizing = req.body;
	Organizing.updateOrganizing(id, organizing, {}, (err, organizing) => {
		if (err) {
			throw err;
		}
		res.json(organizing);
	});
});

app.delete('/api/organizings/:_id', (req, res) => {
	var id = req.params._id;
	Organizing.removeOrganizing(id, (err, organizing) => {
		if (err) {
			throw err;
		}
		res.json(organizing);
	});
});
*/
/*********************************************************
Place
**********************************************************/
/*
app.get('/api/places', (req, res) => {
	Place.getPlaces((err, place) => {
		if (err) {
			throw err;
		}
		res.json(place);
	});
});

app.get('/api/places/:_id', (req, res) => {
	Place.getPlaceById(req.params._id, (err, place) => {
		if (err) {
			throw err;
		}
		res.json(place);
	});
});

app.post('/api/places', (req, res) => {
	var place = req.body;
	Place.addPlace(place, (err, place) => {
		if (err) {
			throw err;
		}
		res.json(place);
	});
});

app.put('/api/places/:_id', (req, res) => {
	var id = req.params._id;
	var place = req.body;
	Place.updatePlace(id, place, {}, (err, place) => {
		if (err) {
			throw err;
		}
		res.json(place);
	});
});

app.delete('/api/places/:_id', (req, res) => {
	var id = req.params._id;
	Place.removePlace(id, (err, place) => {
		if (err) {
			throw err;
		}
		res.json(place);
	});
});
*/
/*********************************************************
Venu
**********************************************************/
/*
app.get('/api/venues', (req, res) => {
	Venue.getVenues((err, venue) => {
		if (err) {
			throw err;
		}
		res.json(venue);
	});
});

app.get('/api/venues/:_id', (req, res) => {
	Venue.getVenueById(req.params._id, (err, venue) => {
		if (err) {
			throw err;
		}
		res.json(venue);
	});
});

app.post('/api/venues', (req, res) => {
	var venue = req.body;
	Venue.addVenue(venue, (err, venue) => {
		if (err) {
			throw err;
		}
		res.json(venue);
	});
});

app.put('/api/venues/:_id', (req, res) => {
	var id = req.params._id;
	var venue = req.body;
	Venue.updateVenue(id, venue, {}, (err, venue) => {
		if (err) {
			throw err;
		}
		res.json(venue);
	});
});

app.delete('/api/venues/:_id', (req, res) => {
	var id = req.params._id;
	Venue.removeVenue(id, (err, venue) => {
		if (err) {
			throw err;
		}
		res.json(venue);
	});
});
*/
/*********************************************************
subevent
**********************************************************/
/*
app.get('/api/subevents', (req, res) => {
	Subevent.getSubevents((err, subevent) => {
		if (err) {
			throw err;
		}
		res.json(subevent);
	});
});

app.get('/api/subevents/:_id', (req, res) => {
	Subevent.getSubeventById(req.params._id, (err, subevent) => {
		if (err) {
			throw err;
		}
		res.json(subevent);
	});
});

app.post('/api/subevents', (req, res) => {
	var subevent = req.body;
	Subevent.addSubevent(subevent, (err, subevent) => {
		if (err) {
			throw err;
		}
		res.json(subevent);
	});
});

app.put('/api/subevents/:_id', (req, res) => {
	var id = req.params._id;
	var subevent = req.body;
	Subevent.updateSubevent(id, subevent, {}, (err, subevent) => {
		if (err) {
			throw err;
		}
		res.json(subevent);
	});
});

app.delete('/api/subevents/:_id', (req, res) => {
	var id = req.params._id;
	Subevent.removeSubevent(id, (err, subevent) => {
		if (err) {
			throw err;
		}
		res.json(subevent);
	});
});
*/
/*********************************************************
tag
**********************************************************/
/*
app.get('/api/tags', (req, res) => {
	Tag.getTags((err, tag) => {
		if (err) {
			throw err;
		}
		res.json(tag);
	});
});

app.get('/api/tags/:_id', (req, res) => {
	Tag.getTagById(req.params._id, (err, tag) => {
		if (err) {
			throw err;
		}
		res.json(tag);
	});
});

app.post('/api/tags', (req, res) => {
	var tag = req.body;
	Tag.addTag(tag, (err, tag) => {
		if (err) {
			throw err;
		}
		res.json(tag);
	});
});

app.put('/api/tags/:_id', (req, res) => {
	var id = req.params._id;
	var tag = req.body;
	Tag.updateTag(id, tag, {}, (err, tag) => {
		if (err) {
			throw err;
		}
		res.json(tag);
	});
});

app.delete('/api/tags/:_id', (req, res) => {
	var id = req.params._id;
	Tag.removeTag(id, (err, tag) => {
		if (err) {
			throw err;
		}
		res.json(tag);
	});
});
//app port listening 
app.listen(3000);
console.log('Running on port 3000...');
*/
