const mongoose = require('mongoose');

// Invite Schema
const inviteSchema = mongoose.Schema({
	/*_id:{
		type: String,
		required: true
	},*/
	userId:{
		type: String,
		required: true,
		ref:'user'
	},
	eventId:{
		type: String,
		required: true,
		ref:'event'
	},
	status:{
		type: String,
		required: true,
		authorized:['accepted','refused']
	}
});

const Invite = module.exports = mongoose.model('Invite', inviteSchema);

// Get Invites
module.exports.getInvites = (callback, limit) => {
	Invite.find(callback).limit(limit);
}

// Get Invite
module.exports.getInviteById = (id, callback) => {
	Invite.findById(id, callback);
}

// Add Invite
module.exports.addInvite = (invite, callback) => {
	Invite.create(invite, callback);
}

// Update Invite
module.exports.updateInvite = (id, invite, options, callback) => {
	var query = {_id: id};
	var update = {
		userId: invite.userId,
		eventId: invite.eventId,
		status: invite.status
	}
	Invite.findOneAndUpdate(query, update, options, callback);
}

// Delete Invite
module.exports.removeInvite = (id, callback) => {
	var query = {_id: id};
	Invite.remove(query, callback);
}
