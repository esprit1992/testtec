const mongoose = require('mongoose');

// Accompany Schema
const accompanySchema = mongoose.Schema({
	//aze
	userId:{
		type: String,
		required: true,
		ref :'user'
	},
	accompagnantId:{
		type: String,
		required: true,
		ref:'user'
	},
	eventId:{
		type: String,
		required : true,
		ref:'event'
	}
});

const Accompany = module.exports = mongoose.model('Accompany', accompanySchema);

// Get Accompanys
module.exports.getAccompanys = (callback, limit) => {
	Accompany.find(callback).limit(limit);
}

// Get Accompany
module.exports.getAccompanyById = (id, callback) => {
	Accompany.findById(id, callback);
}

// Add Accompany
module.exports.addAccompany = (accompany, callback) => {
	Accompany.create(accompany, callback);
}

// Update Accompany
module.exports.updateAccompany = (id, accompany, options, callback) => {
	var query = {_id: id};
	var update = {
		userId: accompany.userId,
		accompagnantId: accompany.accompagnantId
	}
	Accompany.findOneAndUpdate(query, update, options, callback);
}

// Delete Accompany
module.exports.removeAccompany = (id, callback) => {
	var query = {_id: id};
	Accompany.remove(query, callback);
}
