const mongoose = require('mongoose');

// Cathegory Schema
const cathegorySchema = mongoose.Schema({
	/*_id:{
		type: String,
		required: true
	},*/
	name:{
		type: String,
		required: true
	}
});

const Cathegory = module.exports = mongoose.model('Cathegory', cathegorySchema);

// Get Cathegorys
module.exports.getCathegorys = (callback, limit) => {
	Cathegory.find(callback).limit(limit);
}

// Get Cathegory
module.exports.getCathegoryById = (id, callback) => {
	Cathegory.findById(id, callback);
}

// Add Cathegory
module.exports.addCathegory = (cathegory, callback) => {
	Cathegory.create(cathegory, callback);
}

// Update Cathegory
module.exports.updateCathegory = (id, cathegory, options, callback) => {
	var query = {_id: id};
	var update = {
		cathegoryantId: cathegory.cathegoryantId,
		EventId: cathegory.EventId
	}
	Cathegory.findOneAndUpdate(query, update, options, callback);
}

// Delete Cathegory
module.exports.removeCathegory = (id, callback) => {
	var query = {_id: id};
	Cathegory.remove(query, callback);
}
