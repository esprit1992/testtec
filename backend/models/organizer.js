const mongoose = require('mongoose');

// Organizer Schema
const organizerSchema = mongoose.Schema({
	/*_id:{
		type: String,
		required: true
	},*/
	name:{
		type: String,
		required: true
	},
	logo:{
		type: String
	}
});

const Organizer = module.exports = mongoose.model('Organizer', organizerSchema);

// Get Organizers
module.exports.getOrganizers = (callback, limit) => {
	Organizer.find(callback).limit(limit);
}

// Get Organizer
module.exports.getOrganizerById = (id, callback) => {
	Organizer.findById(id, callback);
}

// Add Organizer
module.exports.addOrganizer = (organizer, callback) => {
	Organizer.create(organizer, callback);
}

// Update Organizer
module.exports.updateOrganizer = (id, organizer, options, callback) => {
	var query = {_id: id};
	var update = {
		name: organizer.name,
		logo: organizer.logo
	}
	Organizer.findOneAndUpdate(query, update, options, callback);
}

// Delete Organizer
module.exports.removeOrganizer = (id, callback) => {
	var query = {_id: id};
	Organizer.remove(query, callback);
}
