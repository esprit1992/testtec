const mongoose = require('mongoose');

// Tag Schema
const tagSchema = mongoose.Schema({
	/*_id:{
		type: String,
		required: true
	},*/
	EventId:{
		type: String,
		required: true,
		ref: 'event'
	},
	cathegoryId:{
		type: String,
		required: true,
		ref:'cathegory'
	}
});

const Tag = module.exports = mongoose.model('Tag', tagSchema);

// Get Tags
module.exports.getTags = (callback, limit) => {
	Tag.find(callback).limit(limit);
}

// Get Tag
module.exports.getTagById = (id, callback) => {
	Tag.findById(id, callback);
}

// Add Tag
module.exports.addTag = (tag, callback) => {
	Tag.create(tag, callback);
}

// Update Tag
module.exports.updateTag = (id, tag, options, callback) => {
	var query = {_id: id};
	var update = {
		tagantId: tag.tagantId,
		EventId: tag.EventId
	}
	Tag.findOneAndUpdate(query, update, options, callback);
}

// Delete Tag
module.exports.removeTag = (id, callback) => {
	var query = {_id: id};
	Tag.remove(query, callback);
}
