const mongoose = require('mongoose');

// Sponsoring Schema
const sponsoringSchema = mongoose.Schema({
	/*_id:{
		type: String,
		required: true
	},*/
	sponsorId:{
		type: String,
		ref: 'sponsor',
		required: true
	},
	EventId:{
		type: String,
		ref:'event',
		required: true
	}
});

const Sponsoring = module.exports = mongoose.model('Sponsoring', sponsoringSchema);

// Get Sponsorings
module.exports.getSponsorings = (callback, limit) => {
	Sponsoring.find(callback).limit(limit);
}

// Get Sponsoring
module.exports.getSponsoringById = (id, callback) => {
	Sponsoring.findById(id, callback);
}

// Add Sponsoring
module.exports.addSponsoring = (sponsoring, callback) => {
	Sponsoring.create(sponsoring, callback);
}

// Update Sponsoring
module.exports.updateSponsoring = (id, sponsoring, options, callback) => {
	var query = {_id: id};
	var update = {
		sponsorId: sponsoring.sponsorId,
		EventId: sponsoring.EventId
	}
	Sponsoring.findOneAndUpdate(query, update, options, callback);
}

// Delete Sponsoring
module.exports.removeSponsoring = (id, callback) => {
	var query = {_id: id};
	Sponsoring.remove(query, callback);
}
