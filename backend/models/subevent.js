const mongoose = require('mongoose');

// Subevent Schema
const subeventSchema = mongoose.Schema({
	name:{
		type: String,
		required: true
	},
	creator:{
		type: String
	},
	description:{
		type: String
	},
	venuId:{
		type: String,
		required: true,
		ref:'venu'
	},
	eventId:{
		type:String,
		required: true,
		ref:'event'
	},
	create_date:{
		type: Date,
		default: Date.now
	}
});

const Subevent = module.exports = mongoose.model('Subevent', subeventSchema);

// Get Subevents
module.exports.getSubevents = (callback, limit) => {
	Subevent.find(callback).limit(limit);
}

// Get Subevent
module.exports.getSubeventById = (id, callback) => {
	Subevent.findById(id, callback);
}

// Add Subevent
module.exports.addSubevent = (subevent, callback) => {
	Subevent.create(subevent, callback);
}

// Update Subevent
module.exports.updateSubevent = (id, subevent, options, callback) => {
	var query = {_id: id};
	var update = {
		name: subevent.name,
		creator: subevent.creator,
		description: subevent.description,
		locationLat: subevent.locationLat,
		locationLong: subevent.locationLong,
	}
	Subevent.findOneAndUpdate(query, update, options, callback);
}

// Delete Subevent
module.exports.removeSubevent = (id, callback) => {
	var query = {_id: id};
	Subevent.deleteOne(query, callback);
}
