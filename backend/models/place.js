const mongoose = require('mongoose');

// Place Schema
const placeSchema = mongoose.Schema({
	/*_id:{
		type: String,
		required: true
	},*/
	name:{
		type: String,
		required: true
	},
	lattitude:{
		type: String,
		required: true
	},
	longitude:{
		type: String,
		required: true
	}
});

const Place = module.exports = mongoose.model('Place', placeSchema);

// Get Places
module.exports.getPlaces = (callback, limit) => {
	Place.find(callback).limit(limit);
}

// Get Place
module.exports.getPlaceById = (id, callback) => {
	Place.findById(id, callback);
}

// Add Place
module.exports.addPlace = (place, callback) => {
	Place.create(place, callback);
}

// Update Place
module.exports.updatePlace = (id, place, options, callback) => {
	var query = {_id: id};
	var update = {
		sponsorId: place.sponsorId,
		EventId: place.EventId
	}
	Place.findOneAndUpdate(query, update, options, callback);
}

// Delete Place
module.exports.removePlace = (id, callback) => {
	var query = {_id: id};
	Place.remove(query, callback);
}
