const mongoose = require('mongoose');

// User Schema
const userSchema = mongoose.Schema({
	role:{
		type: String,
		//	required: true
	},
	username:{
		type: String,
		//required: true
	},
	email:{
		type: String,
		//required: true
	},
	fullname:{
		type: String,
		//required: true
	},
	imgUser:{
		type: String,
		//required: true
	},

	gender:{
		type: String
	},
	age:{
		type: String
	}
});

var User = module.exports = mongoose.model('User', userSchema);

// Get Users
module.exports.getUsers = (callback, limit) => {
	User.find(callback).limit(limit);
}

// Get User
module.exports.getUserById = (id, callback) => {
	User.findById(id, callback);
}
// 
module.exports.getUserByMail = (mail, callback) => {
	var query = {email: mail};
	User.findOne(query, callback);
}
// Add User
module.exports.addUser = (user, callback) => {
	User.create(user, callback);
}

// Update User
module.exports.updateUser = (id, user, options, callback) => {
	var query = {_id: id};
	var update = {
		username: user.username,
		email: user.email,
		fullname: user.fullname,
		gender: user.gender,
		age: user.age
	}
	User.findOneAndUpdate(query, update, options, callback);
}


module.exports.getUserByMail = (mail, callback) => {

    var query = {email: mail};

    User.findOne(query, callback);

}
// Delete User
module.exports.removeUser = (id, callback) => {
	var query = {_id: id};
	User.remove(query, callback);
}
