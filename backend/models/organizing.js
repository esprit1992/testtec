const mongoose = require('mongoose');

// Organizing Schema
const organizingSchema = mongoose.Schema({
	/*_id:{
		type: String,
		required: true
	},*/
	organizerId:{
		type: String,
		ref: 'organizer',
		required: true
	},
	EventId:{
		type: String,
		ref:'event',
		required: true
	}
});

const Organizing = module.exports = mongoose.model('Organizing', organizingSchema);

// Get Organizings
module.exports.getOrganizings = (callback, limit) => {
	Organizing.find(callback).limit(limit);
}

// Get Organizing
module.exports.getOrganizingById = (id, callback) => {
	Organizing.findById(id, callback);
}

// Add Organizing
module.exports.addOrganizing = (organizing, callback) => {
	Organizing.create(organizing, callback);
}

// Update Organizing
module.exports.updateOrganizing = (id, organizing, options, callback) => {
	var query = {_id: id};
	var update = {
		sponsorId: organizing.sponsorId,
		EventId: organizing.EventId
	}
	Organizing.findOneAndUpdate(query, update, options, callback);
}

// Delete Organizing
module.exports.removeOrganizing = (id, callback) => {
	var query = {_id: id};
	Organizing.remove(query, callback);
}
