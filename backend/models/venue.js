const mongoose = require('mongoose');

// Venue Schema
const venueSchema = mongoose.Schema({
	/*_id:{
		type: String,
		required: true
	},*/
	placeId:{
		type: String,
		ref: 'place',
		required: true
	},
	EventId:{
		type: String,
		required: true
	}
});

const Venue = module.exports = mongoose.model('Venue', venueSchema);

// Get Venues
module.exports.getVenues = (callback, limit) => {
	Venue.find(callback).limit(limit);
}

// Get Venue
module.exports.getVenueById = (id, callback) => {
	Venue.findById(id, callback);
}

// Add Venue
module.exports.addVenue = (venue, callback) => {
	Venue.create(venue, callback);
}

// Update Venue
module.exports.updateVenue = (id, venue, options, callback) => {
	var query = {_id: id};
	var update = {
		sponsorId: venue.sponsorId,
		EventId: venue.EventId
	}
	Venue.findOneAndUpdate(query, update, options, callback);
}

// Delete Venue
module.exports.removeVenue = (id, callback) => {
	var query = {_id: id};
	Venue.remove(query, callback);
}
