const mongoose = require('mongoose');
var Schema = mongoose.Schema;

//const user = mongoose.model('user', userSchema, 'users');
// Event Schema
const eventSchema = mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	imgEvent: {
		type: String,
		//required: true
	},
	creator: {

		type: Schema.Types.ObjectId,
		ref: 'User'
	},

	description: {
		type: String
	},
	venu: [{
		type: Schema.Types.ObjectId,
		ref: 'User'
	}],
	date: {
		type: Date,
		default: Date.now
	},
	sponsor: [{
		type: Schema.Types.ObjectId,
		ref: 'Sponsor'
	}],


	create_date: {
		type: Date,
		default: Date.now
	}
}, { toJSON: { virtuals: true } });

var Event = module.exports = mongoose.model('Event', eventSchema);

// Get Events
module.exports.getEvents = (callback, limit) => {
	Event.find(callback).limit(limit).populate("User");
}

// get events 2 
module.exports.getEvents = (callback, limit) => {
	Event.find(callback).limit(limit).populate("User");
}
// Get Event
module.exports.getEventById = (id, callback) => {
	Event.findById(id, callback).populate("User");
}

// Add Event
module.exports.addEvent = (event, callback) => {
	
	Event.create(event, callback);
}

// Update Event
module.exports.updateEvent = (id, event, options, callback) => {
	var query = { _id: id };
	var update = {
		name: event.name,
		creator: event.creator,
		description: event.description,
		sponsor: event.sponsor,
		imgEvent: event.imgEvent,
		venu:event.venu,
		date: Date(event.date),
	}
	Event.findOneAndUpdate(query, update, options, callback);
}

// Delete Event
module.exports.removeEvent = (id, callback) => {
	var query = { _id: id };
	Event.deleteOne(query, callback);
}
