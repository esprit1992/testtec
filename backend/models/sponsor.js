const mongoose = require('mongoose');

// Sponsor Schema
const sponsorSchema = mongoose.Schema({
	name:{
		type: String,
		required: true
	},
	logo:{
		type: String
	}
});

var Sponsor=module.exports = mongoose.model('Sponsor', sponsorSchema);

// Get Sponsors
module.exports.getSponsors = (callback, limit) => {
	Sponsor.find(callback).limit(limit);
}

// Get Sponsor
module.exports.getSponsorById = (id, callback) => {
	Sponsor.findById(id, callback);
}

// Add Sponsor
module.exports.addSponsor = (sponsor, callback) => {
	Sponsor.create(sponsor, callback);
}

// Update Sponsor
module.exports.updateSponsor = (id, sponsor, options, callback) => {
	var query = {_id: id};
	var update = {
		name: sponsor.name,
		logo: sponsor.logo
	}
	Sponsor.findOneAndUpdate(query, update, options, callback);
}

// Delete Sponsor
module.exports.removeSponsor = (id, callback) => {
	var query = {_id: id};
	Sponsor.remove(query, callback);
}
