const mongoose = require('mongoose');

// Attend Schema
const attendSchema = mongoose.Schema({
	/*_id:{
		type: String,
		required: true
	},*/
	attendantId:{
		type: String,
		required: true,
		ref:'user'
	},
	EventId:{
		type: String,
		required: true,
		ref:'event'
	}
});

const Attend = module.exports = mongoose.model('Attend', attendSchema);

// Get Attends
module.exports.getAttends = (callback, limit) => {
	Attend.find(callback).limit(limit);
}

// Get Attend
module.exports.getAttendById = (id, callback) => {
	Attend.findById(id, callback);
}

// Add Attend
module.exports.addAttend = (attend, callback) => {
	Attend.create(attend, callback);
}

// Update Attend
module.exports.updateAttend = (id, attend, options, callback) => {
	var query = {_id: id};
	var update = {
		attendantId: attend.attendantId,
		EventId: attend.EventId
	}
	Attend.findOneAndUpdate(query, update, options, callback);
}

// Delete Attend
module.exports.removeAttend = (id, callback) => {
	var query = {_id: id};
	Attend.remove(query, callback);
}
