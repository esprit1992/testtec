var myApp = angular.module('myApp',['ngRoute']);

myApp.config(function($routeProvider){
	$routeProvider.when('/', {
		controller:'EventsController',
		templateUrl: 'views/events.html'
	})
	.when('/events', {
		controller:'EventsController',
		templateUrl: 'views/events.html'
	})
	.when('/events/details/:id',{
		controller:'EventsController',
		templateUrl: 'views/event_details.html'
	})
	.when('/events/add',{
		controller:'EventsController',
		templateUrl: 'views/add_event.html'
	})
	.when('/events/edit/:id',{
		controller:'EventsController',
		templateUrl: 'views/edit_event.html'
	})
	.when('/users', {
		controller:'UsersController',
		templateUrl: 'views/users.html'
	})
	.when('/users/details/:id',{
		controller:'UsersController',
		templateUrl: 'views/user_details.html'
	})
	.when('/users/add',{
		controller:'UsersController',
		templateUrl: 'views/add_user.html'
	})
	.when('/users/edit/:id',{
		controller:'UsersController',
		templateUrl: 'views/edit_user.html'
	})
	.when('/sponsors', {
		controller:'SponsorsController',
		templateUrl: 'views/sponsors.html'
	})
	.when('/sponsors/details/:id',{
		controller:'SponsorsController',
		templateUrl: 'views/sponsor_details.html'
	})
	.when('/sponsors/add',{
		controller:'SponsorsController',
		templateUrl: 'views/add_sponsor.html'
	})
	.when('/sponsors/edit/:id',{
		controller:'SponsorsController',
		templateUrl: 'views/edit_sponsor.html'
	})
	.otherwise({
		redirectTo: '/'
	});
});