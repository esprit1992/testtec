var myApp = angular.module('myApp');

myApp.controller('UsersController', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams){
	console.log('UsersController loaded...');

	$scope.getUsers = function(){
		$http({
			method: 'GET',
			url:'/api/users'
		}).then(function(response){
			$scope.users = response.data;
		},function(error){
			console.log('error');
		});
	}

	$scope.getUser = function(){
		var id = $routeParams.id;
		console.log('id is '+id);
		$http({
			method:'GET',
			url:'/api/users/'+id
		}).then(function(response){
			$scope.user = response.data;
			
		},function(error){
			console.log('error');
		});
		}
	

	$scope.addUser = function(){
		console.log($scope.user);
		$http({
			method: 'POST',
			url:'/api/users/',
			data: $scope.user
		}).then(function(response){
			window.location.href='#!/users';
		},function(error){
			console.log('error');
		});
	}

	$scope.updateUser = function(){
		var id = $routeParams.id;
		$http({
			method: 'put',
			url:'/api/users/'+id,
			data: $scope.user
		}).then(function(response){
			window.location.href='#!/users';
		},function(error){
			console.log('error');
		});
	}

	$scope.removeUser = function(id){
		$http({
			method: 'delete',
			url:'/api/users/'+id,
		}).then(function(response){
			window.location.href='#!/users';
		},function(error){
			console.log('error');
		});
	}
}]);