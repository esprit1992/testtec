var myApp = angular.module('myApp');

myApp.controller('EventsController', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams){
	console.log('EventsController loaded...');


	$scope.getEvents = function(){
		$http({
			method: 'GET',
			url:'/api/events'
		}).then(function(response){
			$scope.events = response.data;
		},function(error){
			console.log('error');
		});
	}
		$scope.getSponsors = function(){
		$http({
			method: 'GET',
			url:'/api/sponsors'
		}).then(function(response){
			$scope.sponsors = response.data;
			console.log('it works!')
		},function(error){
			console.log('error');
		});
	}
		$scope.getUsers = function(){
		$http({
			method: 'GET',
			url:'/api/users'

		}).then(function(response){
			$scope.users = response.data;
			console.log('this works! too ');
		},function(error){
			console.log('error');
		});
	}
	$scope.getEvent = function(){
		var id = $routeParams.id;
		console.log('id is '+id);
		$http({
			method:'GET',
			url:'/api/events/'+id
		}).then(function(response){
			$scope.event = response.data;
			
		},function(error){
			console.log('error');
		});
		}
	

	$scope.addEvent = function(){
		console.log($scope.event);
		$http({
			method: 'POST',
			url:'/api/events/',
			data: $scope.event
		}).then(function(response){
			window.location.href='#!/events';
		},function(error){
			console.log('error');
		});
	}

	$scope.updateEvent = function(){
		var id = $routeParams.id;
		$http({
			method: 'put',
			url:'/api/events/'+id,
			data: $scope.event
		}).then(function(response){
			window.location.href='#!/events';
		},function(error){
			console.log('error');
		});
	}

	$scope.removeEvent = function(id){
		$http({
			method: 'delete',
			url:'/api/events/'+id,
		}).then(function(response){
			window.location.href='#!/events';
		},function(error){
			console.log('error');
		});
	}
}]);