var myApp = angular.module('myApp');

myApp.controller('SponsorsController', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams){
	console.log('SponsorsController loaded...');

	$scope.getSponsors = function(){
		$http({
			method: 'GET',
			url:'/api/sponsors'
		}).then(function(response){
			$scope.sponsors = response.data;
			console.log('it works!')
		},function(error){
			console.log('error');
		});
	}

	$scope.getSponsor = function(){
		var id = $routeParams.id;
		console.log('id is '+id);
		$http({
			method:'GET',
			url:'/api/sponsors/'+id
		}).then(function(response){
			$scope.sponsor = response.data;
			
		},function(error){
			console.log('error');
		});
		}
	

	$scope.addSponsor = function(){
		console.log($scope.sponsor);
		$http({
			method: 'POST',
			url:'/api/sponsors/',
			data: $scope.sponsor
		}).then(function(response){
			window.location.href='#!/sponsors';
		},function(error){
			console.log('error');
		});
	}

	$scope.updateSponsor = function(){
		var id = $routeParams.id;
		$http({
			method: 'put',
			url:'/api/sponsors/'+id,
			data: $scope.sponsor
		}).then(function(response){
			window.location.href='#!/sponsors';
		},function(error){
			console.log('error');
		});
	}

	$scope.removeSponsor = function(id){
		$http({
			method: 'delete',
			url:'/api/sponsors/'+id,
		}).then(function(response){
			window.location.href='#!/sponsors';
		},function(error){
			console.log('error');
		});
	}
}]);